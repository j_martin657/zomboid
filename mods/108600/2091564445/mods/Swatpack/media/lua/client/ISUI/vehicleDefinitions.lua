CheatCoreCM = CheatCoreCM or {}

local modVehicles = {} -- do not modify

--correct format is: modVehicles["MODID"] = { {"display name (can be anything)", "vehicle ID", "category name (can be anything)"} }
--table key must be the modID defined in the mod's modinfo.txt file, i.e modVehicles["FRUsedCars"] for Filibuster Rhyme's used cars.

--definitions go below here. thank you Oh God Spiders No for the existing definitions

modVehicles["Swatpack"] = {
	{"StepVanSwat","base.StepVanSwat", "Swatpack"},
	{"RiotTruck","base.RiotTruck", "Swatpack"},
    {"SwatTruck","base.SwatTruck", "Swatpack"},
    {"BankTruck","base.BankTruck", "Swatpack"},
}


CheatCoreCM.modVehicles = modVehicles -- do not modify. do not place definitions below this