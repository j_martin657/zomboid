if VehicleZoneDistribution then -- check if the table exists for backwards compatibility

-- ****************************** --
--          SPECIAL VEHICLES      --
-- ****************************** --

-- police
VehicleZoneDistribution.police = {};
VehicleZoneDistribution.police.vehicles = {};
VehicleZoneDistribution.police.vehicles["Base.StepVanSwat"] = {index = -1, spawnChance = 70};
VehicleZoneDistribution.police.vehicles["Base.RiotTruck"] = {index = -1, spawnChance = 30};
VehicleZoneDistribution.police.vehicles["Base.SwatTruck"] = {index = -1, spawnChance = 40};
VehicleZoneDistribution.police.vehicles["Base.BankTruck"] = {index = -1, spawnChance = 10};
VehicleZoneDistribution.police.vehicles["Base.PickUpVanLightsPolice"] = {index = 0, spawnChance = 60};
VehicleZoneDistribution.police.vehicles["Base.CarLightsPolice"] = {index = 0, spawnChance = 80};


--junkyard
VehicleZoneDistribution.junkyard = VehicleZoneDistribution.parkingstall or {}
VehicleZoneDistribution.junkyard.vehicles = VehicleZoneDistribution.parkingstall.vehicles or {}
VehicleZoneDistribution.junkyard.vehicles["Base.StepVanSwat"] = {index = -1, spawnChance = 0.3};
VehicleZoneDistribution.junkyard.vehicles["Base.RiotTruck"] = {index = -1, spawnChance = 0.2};
VehicleZoneDistribution.junkyard.vehicles["Base.SwatTruck"] = {index = -1, spawnChance = 0.3};
VehicleZoneDistribution.junkyard.vehicles["Base.BankTruck"] = {index = -1, spawnChance = 0.4};

end