require 'Items/SuburbsDistributions'
require 'Items/ProceduralDistributions'
require "Items/ItemPicker"


---policeLockers


table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Hat_SwatHelmet");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Hat_SWATRiotHelmet");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Glasses_SwatGoggles");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Hat_Balaclava_Swat");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Hat_SwatGasMask");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Hat_SWATNeck");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Vest_BulletSwat");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Gloves_SwatGloves");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Jacket_Swat");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Trousers_Swat");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Shoes_SwatBoots");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "SwatElbowPads");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 4);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "SwatKneePads");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 4);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "SwatShoulderPads");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 4);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "SWATPouch");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 5);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Bag_PoliceBag");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Bag_BigSwatBag");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Hat_PoliceRiotHelmet");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "RiotArmorSuit");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Shoes_RiotBoots");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Gloves_RiotGloves");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "Hat_Antibombhelmet");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "AntibombSuit");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, "AntibombSuitP2");
table.insert(ProceduralDistributions["list"]["PoliceLockers"].items, 1);



---policestorage ammunition

table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, "RubberShells");
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, 20);
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, "RubberShellsBox");
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, 20);
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, "SwatStunGrenade");
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, 10);
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, "SwatFragGrenade");
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, 10);
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, "SwatSmokeGrenade");
table.insert(ProceduralDistributions["list"]["PoliceStorageAmmunition"].items, 10);



---policestorage guns

table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "RiotShotgun");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 10);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "Co2ShortRiotShotgun");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 10);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "RubberShells");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 5);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "RubberShellsBox");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 5);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "SwatStunGrenade");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 5);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "SwatFragGrenade");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 5);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "SwatSmokeGrenade");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 5);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "RiotShieldSwat");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 15);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "RiotShieldPolice");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 20);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "RiotShieldPolice");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 20);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "9mmMp5Clip");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 10);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "9mmMp5Clip");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 10);
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "SwatMP5");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 10);

---policestorage Outfit


table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Hat_SwatHelmet");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Hat_SWATRiotHelmet");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Glasses_SwatGoggles");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Hat_Balaclava_Swat");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Hat_SwatGasMask");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Hat_SWATNeck");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Vest_BulletSwat");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Gloves_SwatGloves");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Jacket_Swat");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Trousers_Swat");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 2);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Shoes_SwatBoots");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "SwatElbowPads");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 4);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "SwatKneePads");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 4);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "SwatShoulderPads");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 4);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "SWATPouch");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 5);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Bag_PoliceBag");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Bag_BigSwatBag");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 3);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Hat_PoliceRiotHelmet");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "RiotArmorSuit");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Shoes_RiotBoots");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Gloves_RiotGloves");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "Hat_Antibombhelmet");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "AntibombSuit");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, "AntibombSuitP2");
table.insert(ProceduralDistributions["list"]["PoliceStorageOutfit"].items, 1);


