-- Referenced from More VHS, check them out
-- https://steamcommunity.com/sharedfiles/filedetails/?id=2671117800

--##################################################################################
-- Item Reference Tables
--##################################################################################



table.insert(ProceduralDistributions["list"]["MechanicShelfTools"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["MechanicShelfTools"].items, 1.2);
table.insert(ProceduralDistributions["list"]["MechanicShelfWheels"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["MechanicShelfWheels"].items, 1.2);
table.insert(ProceduralDistributions["list"]["MechanicShelfOutfit"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["MechanicShelfOutfit"].items, 1.2);
table.insert(ProceduralDistributions["list"]["MechanicShelfSuspension"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["MechanicShelfSuspension"].items, 1.2);
table.insert(ProceduralDistributions["list"]["MechanicShelfBooks"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["MechanicShelfBooks"].items, 1.2);

table.insert(ProceduralDistributions["list"]["StoreCounterCleaning"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["StoreCounterCleaning"].items, 1);
table.insert(ProceduralDistributions["list"]["StoreCounterBags"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["StoreCounterBags"].items, 1);
table.insert(ProceduralDistributions["list"]["CampingStoreGear"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["CampingStoreGear"].items, 2);
table.insert(ProceduralDistributions["list"]["FishingStoreGear"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["FishingStoreGear"].items, 1.5);
table.insert(ProceduralDistributions["list"]["CampingStoreBackpacks"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["CampingStoreBackpacks"].items, 1);

table.insert(ProceduralDistributions["list"]["StoreShelfMechanics"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["StoreShelfMechanics"].items, 1.2);
table.insert(ProceduralDistributions["list"]["GardenStoreTools"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["GardenStoreTools"].items, 1);
table.insert(ProceduralDistributions["list"]["GardenStoreMisc"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["GardenStoreMisc"].items, 1);
table.insert(ProceduralDistributions["list"]["GarageTools"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["GarageTools"].items, 1);
table.insert(ProceduralDistributions["list"]["GarageMechanics"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["GarageMechanics"].items, 1.2);
table.insert(ProceduralDistributions["list"]["GarageMetalwork"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["GarageMetalwork"].items, 1.2);
table.insert(ProceduralDistributions["list"]["CrateRandomJunk"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["CrateRandomJunk"].items, 1);

table.insert(ProceduralDistributions["list"]["CrateTools"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["CrateTools"].items, 1);
table.insert(ProceduralDistributions["list"]["CrateSheetMetal"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["CrateSheetMetal"].items, 1);
table.insert(ProceduralDistributions["list"]["CrateMetalwork"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["CrateMetalwork"].items, 1);
table.insert(ProceduralDistributions["list"]["CrateFarming"].items, "PropaneTank");
table.insert(ProceduralDistributions["list"]["CrateFarming"].items, 1);

