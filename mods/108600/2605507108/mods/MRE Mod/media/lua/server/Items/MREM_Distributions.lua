require 'Items/ProceduralDistributions'

table.insert(ProceduralDistributions["list"]["GunStoreAmmunition"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["GunStoreAmmunition"].items, 10);
table.insert(ProceduralDistributions["list"]["GunStoreShelf"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["GunStoreShelf"].items, 10);
table.insert(ProceduralDistributions["list"]["GunStoreCounter"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["GunStoreCounter"].items, 10);
table.insert(ProceduralDistributions["list"]["GunStoreDisplayCase"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["GunStoreDisplayCase"].items, 10);

table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 5);

table.insert(ProceduralDistributions["list"]["ArmySurplusBackpacks"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["ArmySurplusBackpacks"].items, 5);
table.insert(ProceduralDistributions["list"]["ArmyStorageGuns"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["ArmyStorageGuns"].items, 5);
table.insert(ProceduralDistributions["list"]["LockerArmyBedroom"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["LockerArmyBedroom"].items, 5);

table.insert(ProceduralDistributions["list"]["ClothingStorageWinter"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["ClothingStorageWinter"].items, 10);
table.insert(ProceduralDistributions["list"]["CampingStoreGear"].items, "MREM.MRE");
table.insert(ProceduralDistributions["list"]["CampingStoreGear"].items, 10);


table.insert(ProceduralDistributions["list"]["GunStoreAmmunition"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["GunStoreAmmunition"].items, 5);
table.insert(ProceduralDistributions["list"]["GunStoreShelf"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["GunStoreShelf"].items, 10);
table.insert(ProceduralDistributions["list"]["GunStoreCounter"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["GunStoreCounter"].items, 5);
table.insert(ProceduralDistributions["list"]["GunStoreDisplayCase"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["GunStoreDisplayCase"].items, 5);

table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["PoliceStorageGuns"].items, 1);

table.insert(ProceduralDistributions["list"]["ArmyStorageGuns"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["ArmyStorageGuns"].items, 5);
table.insert(ProceduralDistributions["list"]["LockerArmyBedroom"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["LockerArmyBedroom"].items, 5);
table.insert(ProceduralDistributions["list"]["ArmySurplusBackpacks"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["ArmySurplusBackpacks"].items, 5);

table.insert(ProceduralDistributions["list"]["ClothingStorageWinter"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["ClothingStorageWinter"].items, 5);
table.insert(ProceduralDistributions["list"]["CampingStoreGear"].items, "MREM.MREBox");
table.insert(ProceduralDistributions["list"]["CampingStoreGear"].items, 5);
