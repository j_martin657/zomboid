require "ISUI/ISToolTipInv"


local old_render = ISToolTipInv.render
local item = nil
local numRows = 0

local damageText = nil
local rangeText = nil
local hitText = nil
local critText = nil
local condText = nil
local breakText = nil
local accText = nil
local gunText = nil
local weaponText = "TET"

local old_render = ISToolTipInv.render

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

function ISToolTipInv:render()
	numRows = 0
	if self.item ~= nil then
		item = self.item
		getPlayer()
		if item and instanceof(item, "HandWeapon") then
			numRows = 7
			local weaponLevel = 0
			local category = item:getCategories()
			print(category)
			if category:contains("Axe") then
				weaponLevel = getPlayer():getPerkLevel(Perks.Axe)
				weaponText = "Axe"
			elseif  category:contains("LongBlade") then
				weaponLevel = getPlayer():getPerkLevel(Perks.LongBlade)
				weaponText = "Long Blade"
			elseif  category:contains("SmallBlade") then
				weaponLevel = getPlayer():getPerkLevel(Perks.SmallBlade)
				weaponText = "Short Blade"
			elseif  category:contains("SmallBlunt") then
				weaponLevel = getPlayer():getPerkLevel(Perks.SmallBlunt)
				weaponText = "Short Blunt"
			elseif  category:contains("Blunt") then
				weaponLevel = getPlayer():getPerkLevel(Perks.Blunt)
				weaponText = "Long Blunt"
			elseif  category:contains("Spear") then
				weaponLevel = getPlayer():getPerkLevel(Perks.Spear)
				weaponText = "Spear"
			end
			
			local minDamage = round(item:getMinDamage(), 3)
			local maxDamage = round(item:getMaxDamage(), 3)
			local minRange = round(item:getMinRange(), 3)
			local maxRange = round(item:getMaxRange(), 3)
			local critChance = round(item:getCriticalChance(), 3)
			local critDmg = round(item:getCritDmgMultiplier(), 3)
			local maxHit  = round(item:getMaxHitCount())
			local condition = item:getCondition()
			local conditionMax = item:getConditionMax()
			local conditionLowerChance = item:getConditionLowerChance()
			local knockdown = item:getKnockdownMod()
			-- local knockback = item:getKnockbackMod()
			local maxHitCount = item:getMaxHitCount()
			
			damageText = "Damage: " .. minDamage  .. " - " .. maxDamage .. " (+" .. 30+weaponLevel*10 .. "%)"
			rangeText  = "Range: "  .. minRange   .. " - " .. maxRange 
			critText   = "Crit: "   .. critChance .. "% (+" .. 3*weaponLevel .. "%), " .. critDmg .. "x"
			hitText    = "Max hit: " .. maxHit
			condText   = "Condition: " .. condition .. "/" .. conditionMax
			breakText  = "Break chance: 1/" .. conditionLowerChance
			
			-- Firearm specific stats
			if item:getSubCategory() == "Firearm" then
				numRows = 9
				weaponText = "Firearm"
				weaponLevel = getPlayer():getPerkLevel(Perks.Aiming)
				local hitChance = item:getHitChance()
				local aimingTime = item:getAimingTime()
				local reloadTime = item:getReloadTime()
				local recoilDelay = item:getRecoilDelay()
				
				-- Aiming modified skills
				local critModifier = item:getAimingPerkCritModifier()
				local minAngleModifier = item:getAimingPerkMinAngleModifier()
				local hitChanceModifier = item:getAimingPerkHitChanceModifier()
				local rangeModifier = item:getAimingPerkRangeModifier()
				
				critText  = "Crit: "   .. critChance .. "% (+" .. critModifier*weaponLevel .. "%), " .. critDmg .. "x"
				rangeText = rangeText .. "(+" .. rangeModifier*weaponLevel .. ")"
				accText = "Accuracy: " .. hitChance .. "% (+" .. hitChanceModifier*weaponLevel .. ")%"
				gunText = "Aim/Reload/Recoil time: " .. aimingTime .. "/" .. reloadTime .. "/" .. recoilDelay
			end
		else
			return old_render(self)
		end
	end
	local stage = 1
	local old_y = 0
	local fontSize = 0
	local tooltipFontSize = 0
	local lineSpacing = self.tooltip:getLineSpacing()
	local old_setHeight = self.setHeight
	self.setHeight = function(self, num, ...)
		if stage == 1 then
			stage = 2
			old_y = num
			num = num + numRows * lineSpacing
		else 
			stage = -1 --error
		end
		return old_setHeight(self, num, ...)
	end
	local old_drawRectBorder = self.drawRectBorder
	self.drawRectBorder = function(self, ...)
		if numRows > 0 then
			local color = {0.68, 0.64, 0.96}
			local font = UIFont[getCore():getOptionTooltipFont()];
			self.tooltip:DrawText(font, weaponText, 5, old_y, color[1], color[2], color[3], 1);
			self.tooltip:DrawText(font, damageText, 5, old_y + lineSpacing, color[1], color[2], color[3], 1);
			self.tooltip:DrawText(font, rangeText,  5, old_y + lineSpacing*2,  color[1], color[2], color[3], 1);
			self.tooltip:DrawText(font, hitText,    5, old_y + lineSpacing*3,  color[1], color[2], color[3], 1);
			self.tooltip:DrawText(font, critText,   5, old_y + lineSpacing*4,  color[1], color[2], color[3], 1);
			self.tooltip:DrawText(font, condText,   5, old_y + lineSpacing*5,  color[1], color[2], color[3], 1);
			self.tooltip:DrawText(font, breakText,  5, old_y + lineSpacing*6,  color[1], color[2], color[3], 1);
			if numRows == 9 then
				self.tooltip:DrawText(font, accText,    5, old_y+ lineSpacing*7, color[1], color[2], color[3], 1);
				self.tooltip:DrawText(font, gunText,    5, old_y+ lineSpacing*8, color[1], color[2], color[3], 1);
			end
			stage = 3
		else
			stage = -1 --error
		end
		return old_drawRectBorder(self, ...)
	end
	old_render(self)
	self.setHeight = old_setHeight
	self.drawRectBorder = old_drawRectBorder
end

-- TODO: Add colors based on whether a value is good or bad
--       Add stats based on player skill