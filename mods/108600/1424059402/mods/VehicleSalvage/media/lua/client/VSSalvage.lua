--***********************************************************
--**                       J.Carver                        **
--***********************************************************
require "TimedActions/ISBaseTimedAction"
VSSalvageAction = ISBaseTimedAction:derive("VSSalvageAction");
function VSSalvageAction:isValid()
	return true
end
function VSSalvageAction:update()
end
function VSSalvageAction:start()
       	self.igniteSound = getSoundManager():PlayWorldSoundWav('BT_Ignite', false, self.character:getSquare(), 0, 2, 0, false);
	getSoundManager():PlayWorldSoundWav('BT_Use', false, self.character:getSquare(), 0, 2, 0, false);
end
function VSSalvageAction:stop()
    ISBaseTimedAction.stop(self);
end
function VSSalvageAction:perform()
	getSoundManager():PlayWorldSoundWav('BT_Out', false, self.character:getSquare(), 0, 2, 0, false);
	local name = self.name
	local nameraw = self.nameraw
	if string.match(name, "Burnt") then
		local unburnt = string.gsub(name, "Burnt", "")
		if getTextOrNull("IGUI_VehicleName" .. unburnt) then
			name = getText("IGUI_VehicleName" .. unburnt)
		end
		name = getText("IGUI_VehicleNameBurntCar", name);
	end
	local wreck = false
	if string.match(name, "Burnt") then wreck = true end
	local propaneNeeded = 0.4
	if wreck == false then
		local parts = self.vehicle:getPartCount()
		propaneNeeded = propaneNeeded + (parts * 0.007)
	end
	--LOOT TABLE <MAXIMUM>
	local scrap = 10
	local sPlates = 0
	local lPlates = 0
	local bars = 0
	local pipes = 0
	local redLight = 0
	local yellowLight = 0
	local whiteLight = 4
	local electronics = 0  
	local skillupM = 0
	if string.match(nameraw, "Pickup") then
		if string.match(nameraw, "van") then
			--with topper
			scrap = 2
			sPlates = 2
			lPlates = 3
			bars = 2
			pipes = 2			
		else
			--without topper
			scrap = 1
			sPlates = 2
			lPlates = 2
			bars = 2
			pipes = 2			
		end
	elseif string.match(nameraw, "Wagon") then
		--station wagon
			scrap = 2
			sPlates = 2
			lPlates = 3
			bars = 2
			pipes = 2			
	elseif string.match(nameraw, "Sport") then
		--sports car
			scrap = 1
			sPlates = 2
			bars = 1
			pipes = 2			
	elseif string.match(nameraw, "Small") then 
		-- compact
			scrap = 1
			sPlates = 1 
			bars = 1
			pipes = 1			
	elseif string.match(nameraw, "Off") then
		-- jeep
			scrap = 1
			sPlates = 1
			bars = 1
			pipes = 1			
	elseif string.match(nameraw, "SUV") then
		--SUV 
			scrap = 2
			sPlates = 2
			lPlates = 1
			bars = 2
			pipes = 1			
	elseif string.match(nameraw, "Van") then
		if string.match(nameraw, "Step") then
			--stepvan
			scrap = 4
			sPlates = 4
			lPlates = 4 --100
			bars = 3
			pipes = 2			
		else
			-- passengervan
			-- cargo van
			scrap = 2
			sPlates = 2
			lPlates = 3
			bars = 2
			pipes = 2			
		end
	elseif string.match(nameraw, "Ambulance") then
			scrap = 2
			sPlates = 2
			lPlates = 3
			bars = 2 
			pipes = 2			
	else
		-- Sedan
			scrap = 2
			sPlates = 2
			lPlates = 1
			bars = 2
			pipes = 2			
	end
	--Specials
	if string.match(nameraw, "Police") then
		--police vehicle
		redLight = 4
		electronics = 2
	end
	if string.match(nameraw, "Lights") then
		--lighbar
		yellowLight = 2
	end
	if string.match(nameraw, "Radio") then
		--news vehicle
		electronics = 4
	end 
	if string.match(nameraw, "Special") then
		-- special vehicle
	end
-- turn items to scrap based on skill and random
	--MetalWorking fail turns item to scrap
	local failChanceW = (((100 - (self.skillW * 10)) + 4) / 2) -- always a 4% chance of fail
	local success = 0
	local fails = 0
	for i=1,sPlates do
		success = ZombRand(101)
		if success < failChanceW or success > (100 - failChanceW) then fails = fails + 1 end
	end
	scrap = scrap + fails
	sPlates = sPlates - fails		
	fails = 0
	for i=1,lPlates do
		success = ZombRand(101)
		if success < failChanceW or success > (100 - failChanceW) then fails = fails + 1 end
	end
	scrap = scrap + fails 
	lPlates = lPlates - fails		
	fails = 0
	for i=1,bars do
		success = ZombRand(101)
		if success < failChanceW or success > (100 - failChanceW) then fails = fails + 1 end
	end
	scrap = scrap + fails
	bars = bars - fails		
	fails = 0
	for i=1,pipes do
		success = ZombRand(101)
		if success < failChanceW or success > (100 - failChanceW) then fails = fails + 1 end
	end
	scrap = scrap + fails
	pipes = pipes - fails		
	-- chance to recover parts on wreck
	if wreck then
		--Electrical fail returns no item 
		local failChanceE = (((100 - (self.skillE * 10)) + 4) / 2) -- always a 4% chance of fail
		fails = 0 
		for i=1,electronics do
			success = ZombRand(101)
			if success < failChanceE or success > (100 - failChanceE) then fails = fails + 1 end
		end
		electronics = electronics - fails		
		--Mechanics fail returns no item
		local failChanceM = (((100 - (self.skillM * 10)) + 4) / 2) -- always a 4% chance of fail
		fails = 0
		for i=1,whiteLight do
			success = ZombRand(101)
			if success < failChanceM or success > (100 - failChanceM) then fails = fails + 1 end
		end
		whiteLight = whiteLight - fails
		fails = 0
		for i=1,yellowLight do
			success = ZombRand(101)
			if success < failChanceM or success > (100 - failChanceM) then fails = fails + 1 end
		end
		yellowLight = yellowLight - fails
		fails = 0
		for i=1,redLight do --200
			success = ZombRand(101)
			if success < failChanceM or success > (100 - failChanceM) then fails = fails + 1 end
		end
		redLight = redLight - fails
		-- Engine Parts, Brakes, Suspension
		local condForPart = math.max(20 - (self.skillM), 5);
		condForPart = ZombRand(condForPart/3, condForPart);
		if ZombRand(100) < (self.skillM * 5) then
			local itemCond = self.vehicle:getSquare():AddWorldInventoryItem("Base.EngineParts", 0.0,0.0,0.0);
			skillupM = skillupM + 5
			itemCond:setCondition(ZombRand(46) + 5)			
		end
		-- Brakes
		local quality = ZombRand(100)
		for i=1,4 do
			if ZombRand(100) < (self.skillM * 4) then 
				skillupM = skillupM + 1
				local itemName = "Base.NormalBrake1"
				if quality > 75 then
					itemName = "Base.ModernBrake1"
				elseif quality < 50 then
					itemName = "Base.OldBrake1"
				end
				local itemCond = self.vehicle:getSquare():AddWorldInventoryItem(itemName, 0.0,0.0,0.0);
				itemCond:setCondition(ZombRand(46) + 5)			
			end
		end
		-- suspension
		for i=1,4 do
			if ZombRand(100) < (self.skillM * 3) then
				skillupM = skillupM + 3
				local itemCond = self.vehicle:getSquare():AddWorldInventoryItem("Base.NormalSuspension1", 0.0,0.0,0.0);
				if quality > 75 then
					itemCond = self.vehicle:getSquare():AddWorldInventoryItem("Base.ModernSuspension1", 0.0,0.0,0.0);
				end
				itemCond:setCondition(ZombRand(46) + 5)			
			end
		end
		for i=1,whiteLight do
			self.vehicle:getSquare():AddWorldInventoryItem('Base.LightBulb', 0.0, 0.0, 0.0)
		end
		for i=1,yellowLight do
			self.vehicle:getSquare():AddWorldInventoryItem('Base.LightBulbYellow', 0.0, 0.0, 0.0)
		end
		for i=1,redLight do
			self.vehicle:getSquare():AddWorldInventoryItem('Base.LightBulbRed', 0.0, 0.0, 0.0)
		end --250
		for i=1,electronics do
			self.vehicle:getSquare():AddWorldInventoryItem('Base.ElectronicsScrap', 0.0, 0.0, 0.0)
		end
		else
		-- Not a wreck
		local parts = self.vehicle:getPartCount()
		if parts > 0 then
			for i=1,self.vehicle:getPartCount() do
				local part = self.vehicle:getPartByIndex(i-1)
				local cond = part:getCondition() - (20 - self.skillM);
				if part:getItemType() and not part:getItemType():isEmpty() and not part:getInventoryItem() then
					cond = 0;
				end
				if cond > 0 then
					local item = part:getInventoryItem()
					if item then
						local moduleName = item:getModule()
						local itemName = string.gsub(item:getType(), "1", "") 
						local loadName = moduleName .. '.' .. itemName
						local itemCond = item:getCondition();
						if itemCond - (20 - self.skillM) > 0 then	
							if not string.match(itemName , "Door") and not string.match(itemName , "Trunk") and not string.match(itemName,"Glove") then
								skillupM = skillupM + 3
								local itemTemp = self.vehicle:getSquare():AddWorldInventoryItem(item,0.0,0.0,0.0);
								itemTemp:setCondition(itemCond - (20 - self.skillM))
							else
								if string.match(itemName , "Trunk") or string.match(itemName , "Hood") then
									lPlates = lPlates + 1
								else
									sPlates = sPlates + 1
								end
							end
						end
					end
				else
					scrap = scrap + 1
				end
			end
		end
	end
--Create Items on ground
	for i=1,scrap do
		self.vehicle:getSquare():AddWorldInventoryItem('Base.ScrapMetal', 0.0, 0.0, 0.0)
	end
	for i=1,sPlates do
		self.vehicle:getSquare():AddWorldInventoryItem('Base.SmallSheetMetal', 0.0, 0.0, 0.0)
	end
	for i=1,lPlates do
		self.vehicle:getSquare():AddWorldInventoryItem('Base.SheetMetal', 0.0, 0.0, 0.0)
	end
	for i=1,bars do
		self.vehicle:getSquare():AddWorldInventoryItem('Base.MetalBar', 0.0, 0.0, 0.0)
	end
	for i=1,pipes do
		self.vehicle:getSquare():AddWorldInventoryItem('Base.MetalPipe', 0.0, 0.0, 0.0)
	end

	local args = { vehicle = self.vehicle:getId()}
	if isClient() then
		sendClientCommand(self.character, 'vehicle', 'remove', args)
	elseif isServer() then
		self:remove(player , args)
	else
-- print("Single Player")
		self.vehicle:permanentlyRemove()
	end
-- Add Experience
	local skillupW = (sPlates * 4) + (lPlates * 5) + (bars * 3) + (pipes * 2) + scrap
        self.character:getXp():AddXPNoMultiplier(Perks.MetalWelding, skillupW)
        self.character:getXp():AddXPNoMultiplier(Perks.Mechanics, skillupM)
        self.character:getXp():AddXPNoMultiplier(Perks.Electricity, (electronics * 5))
-- use propane
	local blowtorch = self.character:getInventory():FindAndReturn("BlowTorch")
	blowtorch:setUsedDelta(blowtorch:getUsedDelta() - propaneNeeded )
	ISBaseTimedAction.perform(self);
end
function VSSalvageAction:new (character, vehicle, time)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = character;
	o.stopOnWalk = true;
	o.stopOnRun = true; 
	-- custom fields
	o.name = vehicle:getScript():getName()
	o.nameraw = vehicle:getScript():getName()
	o.skillW = character:getPerkLevel(Perks.MetalWelding)
	o.skillM = character:getPerkLevel(Perks.Mechanics)
	o.skillE = character:getPerkLevel(Perks.Electrical)
	o.vehicle = vehicle
	o.maxTime = time - (character:getPerkLevel(Perks.Mechanics) * (time/15));
	if ISVehicleMechanics.cheat then o.maxTime = 1; end
	o.jobType = "Salvaging"
	--sound fields
	o.igniteSound = getSoundManager():PlayWorldSoundWav('BT_Ignite', false, character:getSquare(), 0, 2, 0.25, false);
	return o
end
