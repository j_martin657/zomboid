--***********************************************************
--**                       J.Carver                        **
--***********************************************************
VSVehicleMenu = {}
require "VSSalvage"
require "ISUI/ISModalDialog"
function VSVehicleMenu.OnFillWorldObjectContextMenu(player, context, worldobjects, test)
	local playerObj = getSpecificPlayer(player)
	local vehicle = playerObj:getVehicle()
	if not vehicle then
		if JoypadState.players[player+1] then
			local px = playerObj:getX()
			local py = playerObj:getY()
			local pz = playerObj:getZ()
			local sqs = {}
			sqs[1] = getCell():getGridSquare(px, py, pz)
			local dir = playerObj:getDir()
			if (dir == IsoDirections.N) then        sqs[2] = getCell():getGridSquare(px-1, py-1, pz); sqs[3] = getCell():getGridSquare(px, py-1, pz);   sqs[4] = getCell():getGridSquare(px+1, py-1, pz);
			elseif (dir == IsoDirections.NE) then   sqs[2] = getCell():getGridSquare(px, py-1, pz);   sqs[3] = getCell():getGridSquare(px+1, py-1, pz); sqs[4] = getCell():getGridSquare(px+1, py, pz);
			elseif (dir == IsoDirections.E) then    sqs[2] = getCell():getGridSquare(px+1, py-1, pz); sqs[3] = getCell():getGridSquare(px+1, py, pz);   sqs[4] = getCell():getGridSquare(px+1, py+1, pz);
			elseif (dir == IsoDirections.SE) then   sqs[2] = getCell():getGridSquare(px+1, py, pz);   sqs[3] = getCell():getGridSquare(px+1, py+1, pz); sqs[4] = getCell():getGridSquare(px, py+1, pz);
			elseif (dir == IsoDirections.S) then    sqs[2] = getCell():getGridSquare(px+1, py+1, pz); sqs[3] = getCell():getGridSquare(px, py+1, pz);   sqs[4] = getCell():getGridSquare(px-1, py+1, pz);
			elseif (dir == IsoDirections.SW) then   sqs[2] = getCell():getGridSquare(px, py+1, pz);   sqs[3] = getCell():getGridSquare(px-1, py+1, pz); sqs[4] = getCell():getGridSquare(px-1, py, pz);
			elseif (dir == IsoDirections.W) then    sqs[2] = getCell():getGridSquare(px-1, py+1, pz); sqs[3] = getCell():getGridSquare(px-1, py, pz);   sqs[4] = getCell():getGridSquare(px-1, py-1, pz);
			elseif (dir == IsoDirections.NW) then   sqs[2] = getCell():getGridSquare(px-1, py, pz);   sqs[3] = getCell():getGridSquare(px-1, py-1, pz); sqs[4] = getCell():getGridSquare(px, py-1, pz);
			end
			for _,sq in ipairs(sqs) do
				vehicle = sq:getVehicleContainer()
				if vehicle then
					return VSVehicleMenu.FillMenuOutsideVehicle(player, context, vehicle, test)
				end
			end
			return
		end
		vehicle = IsoObjectPicker.Instance:PickVehicle(getMouseXScaled(), getMouseYScaled())
		if vehicle then
			return VSVehicleMenu.FillMenuOutsideVehicle(player, context, vehicle, test)
		end
		return
	end
end

function VSVehicleMenu.FillMenuOutsideVehicle(player, context, vehicle, test)
	local playerObj = getSpecificPlayer(player)
	local tools = false
-- req blowtorch and welding mask equipped
	if (playerObj:getInventory():contains("BlowTorch") and playerObj:getInventory():contains("WeldingMask")) or ISBuildMenu.cheat then
		tools = true
	end
	if tools then
		local blowtorch = playerObj:getInventory():FindAndReturn("BlowTorch")
		local blowTorchUseLeft = blowtorch:getDrainableUsesInt();
		if blowTorchUseLeft >= 20 then
			context:addOption("Salvage Vehicle" , playerObj, VSVehicleMenu.onSalvage, vehicle);
		end
	end
end

function VSVehicleMenu.onSalvage(player , vehicle)
	-- non wreck dialog
	local name = vehicle:getScript():getName()
	if not string.match(name, "Burnt") then
		local parts = vehicle:getPartCount()
		if parts > 0 then
			local playerNum = player:getPlayerNum()
			local text = 'This is not a wrecked vehicle.\nSalvaging may destroy any parts left on the vehicle\nand will destroy any contents.\n\nDo you still wish to salvage?'
			local modal = ISModalDialog:new(0 ,0 ,250 ,150 ,text ,true ,vehicle , VSVehicleMenu.onSalvageNonWreck, playerNum ,parts,player);
			modal:initialise();
			modal:addToUIManager();
		end
	else
		ISTimedActionQueue.add(VSSalvageAction:new(player, vehicle, 1150))
	end
end

function VSVehicleMenu.onSalvageNonWreck(vehicle, button, parts , player )
	if button.internal == "YES" then
		ISTimedActionQueue.add(VSSalvageAction:new(player, vehicle, 1150))
	end
end
Events.OnFillWorldObjectContextMenu.Add(VSVehicleMenu.OnFillWorldObjectContextMenu)