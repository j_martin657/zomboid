require "Items/ProceduralDistributions"


--Bookstore (BookstoreBooks)
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookAxe1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookAxe2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.8);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookAxe3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookAxe4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookAxe5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSpear1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSpear2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.8);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSpear3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSpear4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSpear5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlade1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlade2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.8);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlade3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlade4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlade5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookLongBlade1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookLongBlade2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.8);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookLongBlade3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookLongBlade4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookLongBlade5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookBlunt1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookBlunt2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.8);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookBlunt3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookBlunt4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookBlunt5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlunt1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlunt2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.8);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlunt3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlunt4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKT.BookSmallBlunt5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.2);

--Post Office (PostOfficeBooks)
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookAxe1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookAxe2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookAxe3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookAxe4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookAxe5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSpear1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSpear2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSpear3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSpear4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSpear5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlade1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlade2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlade3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlade4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlade5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookLongBlade1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookLongBlade2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookLongBlade3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookLongBlade4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookLongBlade5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookBlunt1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookBlunt2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookBlunt3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookBlunt4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookBlunt5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlunt1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.6);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlunt2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlunt3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlunt4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKT.BookSmallBlunt5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);

--Library Books (LibraryBooks)
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookAxe1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.9);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookAxe2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookAxe3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookAxe4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookAxe5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSpear1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.9);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSpear2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSpear3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSpear4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSpear5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlade1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.9);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlade2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlade3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlade4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlade5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookLongBlade1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.9);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookLongBlade2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookLongBlade3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookLongBlade4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookLongBlade5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookBlunt1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.9);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookBlunt2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookBlunt3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookBlunt4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookBlunt5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlunt1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.9);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlunt2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlunt3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlunt4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKT.BookSmallBlunt5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.1);

--Living Room(LivingRoomShelf)
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookAxe1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookAxe2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.4);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookAxe3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookAxe4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookAxe5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSpear1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSpear2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.4);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSpear3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSpear4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSpear5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlade1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlade2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.4);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlade3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlade4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlade5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookLongBlade1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookLongBlade2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.4);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookLongBlade3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookLongBlade4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookLongBlade5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookBlunt1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookBlunt2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.4);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookBlunt3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookBlunt4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookBlunt5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.1);

table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlunt1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlunt2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.4);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlunt3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlunt4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKT.BookSmallBlunt5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.1);