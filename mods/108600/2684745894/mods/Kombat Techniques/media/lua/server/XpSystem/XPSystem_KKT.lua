SkillBook["Axe"] = {};
SkillBook["Axe"].perk = Perks.Axe;
SkillBook["Axe"].maxMultiplier1 = 3;
SkillBook["Axe"].maxMultiplier2 = 5;
SkillBook["Axe"].maxMultiplier3 = 8;
SkillBook["Axe"].maxMultiplier4 = 12;
SkillBook["Axe"].maxMultiplier5 = 16;

SkillBook["Spear"] = {};
SkillBook["Spear"].perk = Perks.Spear;
SkillBook["Spear"].maxMultiplier1 = 3;
SkillBook["Spear"].maxMultiplier2 = 5;
SkillBook["Spear"].maxMultiplier3 = 8;
SkillBook["Spear"].maxMultiplier4 = 12;
SkillBook["Spear"].maxMultiplier5 = 16;

SkillBook["SmallBlade"] = {};
SkillBook["SmallBlade"].perk = Perks.SmallBlade;
SkillBook["SmallBlade"].maxMultiplier1 = 3;
SkillBook["SmallBlade"].maxMultiplier2 = 5;
SkillBook["SmallBlade"].maxMultiplier3 = 8;
SkillBook["SmallBlade"].maxMultiplier4 = 12;
SkillBook["SmallBlade"].maxMultiplier5 = 16;

SkillBook["LongBlade"] = {};
SkillBook["LongBlade"].perk = Perks.LongBlade;
SkillBook["LongBlade"].maxMultiplier1 = 3;
SkillBook["LongBlade"].maxMultiplier2 = 5;
SkillBook["LongBlade"].maxMultiplier3 = 8;
SkillBook["LongBlade"].maxMultiplier4 = 12;
SkillBook["LongBlade"].maxMultiplier5 = 16;

SkillBook["Blunt"] = {};
SkillBook["Blunt"].perk = Perks.Blunt;
SkillBook["Blunt"].maxMultiplier1 = 3;
SkillBook["Blunt"].maxMultiplier2 = 5;
SkillBook["Blunt"].maxMultiplier3 = 8;
SkillBook["Blunt"].maxMultiplier4 = 12;
SkillBook["Blunt"].maxMultiplier5 = 16;

SkillBook["SmallBlunt"] = {};
SkillBook["SmallBlunt"].perk = Perks.SmallBlunt;
SkillBook["SmallBlunt"].maxMultiplier1 = 3;
SkillBook["SmallBlunt"].maxMultiplier2 = 5;
SkillBook["SmallBlunt"].maxMultiplier3 = 8;
SkillBook["SmallBlunt"].maxMultiplier4 = 12;
SkillBook["SmallBlunt"].maxMultiplier5 = 16;