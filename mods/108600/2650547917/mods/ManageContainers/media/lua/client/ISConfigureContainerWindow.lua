require "ISUI/ISCollapsableWindow"
require "ISUI/ISTabPanel"
require "ISUI/ISTextEntryBox"
require "ItemCategoryListbox"

local closingWindow = false

ISConfigureContainerWindow = ISCollapsableWindow:derive("ISConfigureContainerWindow")
ISConfigureContainerWindow.windows = {}
ISConfigureContainerWindow.leftCategory = Keyboard.KEY_LEFT;
ISConfigureContainerWindow.rightCategory = Keyboard.KEY_RIGHT;
ISConfigureContainerWindow.upArrow = Keyboard.KEY_UP;
ISConfigureContainerWindow.downArrow = Keyboard.KEY_DOWN;
ISConfigureContainerWindow.FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
ISConfigureContainerWindow.containerDataPrefix = 'manageContainerData_'

function ISConfigureContainerWindow:createChildren()
	ISCollapsableWindow.createChildren(self)
	local titleBarHeight = self:titleBarHeight()

	self:setInfo(getText("UI_ConfigureContainer"));

	self.infoButton = ISButton:new(self.closeButton:getRight() + 1, 15, 70, 1, getText("UI_InfoBtn"), self, self.onOptionMouseDown);

	local inset = 4
	local textboxPadding = 8
	local textboxHeight = self.FONT_HGT_SMALL
	self.textPanel = ISPanel:new(0, titleBarHeight, self:getWidth(), textboxHeight+inset)
	self.textPanel:initialise();
	self:addChild(self.textPanel);

	local lblText = getText("IGUI_Name") .. ":"
	local lbl = ISLabel:new(4, 2, textboxHeight, lblText, 1, 1, 1, 1.0, UIFont.Small, true);
	lbl:initialise();
	lbl:instantiate();
	self.textPanel:addChild(lbl);


	local textboxX = getTextManager():MeasureStringX(UIFont.Small, lblText) + textboxPadding
	self.textBox = ISTextEntryBox:new("", textboxX, 0, self:getWidth()-textboxX, textboxHeight)
	self.textBox:initialise()
	self.textBox:setAnchorRight(true)
	self.textBox:setAnchorTop(true)
	self.textPanel:addChild(self.textBox)

	local tabStart = titleBarHeight+self.textPanel:getY()+(inset/2)
	self.panel = ISTabPanel:new(0, tabStart, self:getWidth(), self:getHeight()-tabStart)
	self.panel:initialise();
	self.panel.target = self
	self.panel.onActivateView = self.onActivateView
	self.panel:setEqualTabWidth(false)
	self:addChild(self.panel)

	self.listBox = ItemCategoryListbox:new(0, self.panel.tabHeight, self:getWidth(), self:getHeight()-(tabStart+self.panel.tabHeight))
	self.listBox:initialise();
  self.listBox:instantiate();
	--self.listBox:setAnchorLeft(true)
	self.panel:addChild(self.listBox)

	local categories = self.listBox:GetCategories()
	for j=1,categories:size() do
		local category = categories:get(j-1)
		local categoryTransKey = "IGUI_ItemCat_" .. category
		local categoryTrans = getText(categoryTransKey)
		--If the translation did not pull, show key val
		categoryTrans = categoryTrans == categoryTransKey and category or categoryTrans
		self.listBox:addItem(categoryTrans, category)
	end
	self.listBox:sort()

	self:setObject(self.containers)

	self.panel:activateView(self:getContainerName(self.containers:get(0)))
end

function ISConfigureContainerWindow:addPanels(addNum)
	for i=0, addNum-1 do
		--lets say I had 2  and now I have 5, I need to get index 2,3,4
		local dummyElem = ISUIElement:new(0, 0, 0, 0)
		local container = self.containers:get(self.containers:size() - addNum + i)
		self.panel:addView(self:getContainerName(container), dummyElem)
	end
end

function ISConfigureContainerWindow:removePanels(removeNum)
	--lets say I had 4  and now I have 1, I need to get index 1,2,3
	for i=0, removeNum-1 do
		local removeIndex = self.containers:size() - removeNum + i
		self.panel:removeView(self.panel:getView(self:getContainerName(self.containers:get(removeIndex))))
	end
end

function ISConfigureContainerWindow:setObject(newContainers)
	closingWindow = false
	self:saveChanges()
	self.prevContainerSelection = 0
	local diffContainers = newContainers:size() - self.prevContainersCount
	--reset view names
	for i,curView in ipairs(self.panel.viewList) do
		-- If we have 1 new container and there's more than 1 view,
		if (newContainers:size() < i) then
			break
		end
		curView.name = self:getContainerName(newContainers:get(i-1))
		local newWidth = getTextManager():MeasureStringX(UIFont.Small, curView.name) + self.panel.tabPadX
		curView.tabWidth = newWidth;
	end

	if (diffContainers > 0) then
		self.containers = newContainers
		self:addPanels(diffContainers)
	else
		self:removePanels(diffContainers*-1)
		self.containers = newContainers
	end
	self.panel:activateView(self:getContainerName(self.containers:get(self.containers:size()-1)))
	self.prevContainersCount = self.containers:size()
	self:refreshSelection()
	if JoypadState.players[self.playerNum+1] then
		setJoypadFocus(self.playerNum, self.listbox)
	end
end

function ISConfigureContainerWindow:close()
	closingWindow = true
	self:saveChanges()
	getPlayer():setIgnoreAimingInput(false);
	self:removeFromUIManager()
	if JoypadState.players[self.playerNum+1] then
		setJoypadFocus(self.playerNum, nil)
	end
end

function ISConfigureContainerWindow:toggleOption(listboxIndex)
	self.listBox:setRow(listboxIndex, true)
end

function ISConfigureContainerWindow:onJoypadDown(button)
	if button == Joypad.BButton then
		self:close()
	end
	if button == Joypad.AButton then
		self:toggleOption()
	end
	if button == Joypad.LBumper or button == Joypad.RBumper then
		local nextView = self.panel:getActiveViewIndex()-1

		if button == Joypad.LBumper then
			nextView = nextView - 1
			if nextView < 0 then
				nextView = self.containers:size()-1
			end
		end

		if button == Joypad.RBumper then
			nextView = nextView + 1
			if nextView > self.containers:size()-1 then
				nextView = 0
			end
		end


		self.panel:activateView(self:getContainerName(self.containers:get(nextView)))
		self.listBox:selectEarliest()
	end
end

function ISConfigureContainerWindow:onJoypadDirDown(joypadData)
	self.listBox:onJoypadDirDown();
end

function ISConfigureContainerWindow:onJoypadDirUp(joypadData)
	self.listBox:onJoypadDirUp();
end

function ISConfigureContainerWindow:onGainJoypadFocus(joypadData)
	self.listBox:selectEarliest()
end

--TODO: Find why this even does not trigger each time a view is clicked
function ISConfigureContainerWindow:onActivateView()
	if self.prevContainerSelection > 0 then
		self:saveChanges(self.prevContainerSelection)
	end
	self:refreshSelection()
	self.prevContainerSelection = self.panel:getActiveViewIndex()
end

function ISConfigureContainerWindow:refreshSelection()
	local containerData = self.containers:get(0):getParent():getModData()[self.containerDataPrefix .. self.playerContainerID]
	if (containerData == nil) then
		containerData = {containerName="", containersFilters={}}
	end
	self.textBox:setText(containerData.containerName)

	self.listBox:clearSelection()
	local listboxValues = containerData.containersFilters[self.panel:getActiveViewIndex()]
	if (listboxValues ~= nil) then
		self.listBox:setSelectedByValues(listboxValues)
	end
end

function ISConfigureContainerWindow:update()
	local containerObj = self.containers:get(0):getParent()
	if self.character:DistTo(containerObj:getX(), containerObj:getY()) > self.closeWinDistance then
			self:setVisible(false);
			self:removeFromUIManager();
	end
end

function ISConfigureContainerWindow:saveChanges(containerIndex)
	if self.prevContainersCount == 0 then return end
	if containerIndex == nil then containerIndex = self.panel:getActiveViewIndex() end

	local data = {}

	for _,selCat in ipairs(self.listBox:getSelectedItems()) do
		table.insert(data, selCat.item)
	end
	local containerObj = self.containers:get(0)
	local parentContainer = containerObj:getParent()
	if parentContainer == nil then --It's possible the obj despawned because too far
		return
	end
	local modData = parentContainer:getModData()
	local containerKey = self.containerDataPrefix .. self.playerContainerID
	if modData[containerKey] == nil then
		modData[containerKey] = {}
	end
	if modData[containerKey]["containersFilters"] == nil then
		modData[containerKey]["containersFilters"] = {}
	end
	modData[containerKey]["containersFilters"][containerIndex] = data
	modData[containerKey]["containerName"] = self.textBox:getText()
	parentContainer:transmitModData()
end

function ISConfigureContainerWindow:new(x, y, character, containers)
	local title = getText("ContextMenu_Configure_Container")
	local iconSpaceTitleRatio = 4.5
	local titlePadding = 20
	local width = math.max(getTextManager():MeasureStringX(UIFont.Small, title) + (getTextManager():getFontHeight(UIFont.Small) * iconSpaceTitleRatio) + titlePadding, 250)
	local height = 600
	local o = ISCollapsableWindow:new(x, y, width, height)
	setmetatable(o, self)
	self.__index = self
	o.title = title
	o.infoBtn = getTexture("media/ui/Panel_info_button.png")
	o.character = character
	o.playerNum = character:getPlayerNum()
	o.playerContainerID = self:getStoredUUID(character, "playerContainerID", true)
	o.containers = containers
	o.closeWinDistance = 10
	o.prevContainersCount = 0
	o.prevContainerSelection = 0
	o:setResizable(false)

	return o
end

function ISConfigureContainerWindow:onMouseMove(dx, dy)
	ISCollapsableWindow.onMouseMove(self,dx,dy);
	getPlayer():setIgnoreAimingInput(self:isMouseOver() and not closingWindow);
end

function ISConfigureContainerWindow:onMouseMoveOutside(dx, dy)
	ISCollapsableWindow.onMouseMoveOutside(self,dx,dy);
	getPlayer():setIgnoreAimingInput(false);
end

function ISConfigureContainerWindow:getStoredUUID(isoObj, uuidName, createOnNil)
	--Used to be stored in the isoObj, but now these values should be fetched in a ini file
	createOnNil = createOnNil or false
	local storedData = LIP:load("manageContainers.ini", createOnNil)
	local iniUUID = storedData.playerData ~= nil and storedData.playerData[uuidName] or nil
	local isoObjUUID = isoObj:getModData()[uuidName]

	-- If they are using the isoObj UUID, don't disturb it
	local storedUUID = isoObjUUID or iniUUID

	--Create new UUID
	if storedUUID == nil and createOnNil then
		storedUUID = getRandomUUID()
	elseif not createOnNil then
		return ''
	end

	if iniUUID == nil then
		LIP:save("manageContainers.ini", {playerData={[uuidName]=storedUUID}})
	end
	if isoObjUUID == nil then
		isoObj:getModData()[uuidName] = storedUUID
	end

	return storedUUID
end

function ISConfigureContainerWindow:getContainerName(container)
	return getTextOrNull("IGUI_ContainerTitle_" .. container:getType())
end