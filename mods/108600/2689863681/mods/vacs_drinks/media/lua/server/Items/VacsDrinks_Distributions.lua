require "Items/ProceduralDistributions"
require "Items/Distributions"

local i, j, k, v

local enableImmersion = VACS_DRINKS_MO.OPTIONS.box1
local removePopSpawns = VACS_DRINKS_MO.OPTIONS.box2
local spawnMisc = VACS_DRINKS_MO.OPTIONS.box3

local alcoholspawnTable = {
  "BandPracticeFridge", 2.2,
  "BandPracticeFridge", 1.1,
  "BarCounterLiquor", 1.1,
  "BarCounterWeapon", 1.1,
  "BarShelfLiquor", 1.1,
  "BreweryCans", 1.1,
  "FridgeBeer", 2.2,
  "FridgeBeer", 1.1,
  "FridgeGeneric", 0.1,
  "FridgeTrailerPark", 0.4,
  "KitchenBottles", 0.2,
  "StoreShelfBeer", 1.1,
}

local alcoholitemTable = {}

if(enableImmersion)
then
  print("VDK Vacs Drinks | Using 1993 drinks only (Alcohol) | 1993=",enableImmersion)
  alcoholitemTable = {
    "VDK.VAC_Can_Budlight", 1,
    "VDK.VAC_Can_Budweiser", 0.6,
    "VDK.VAC_Can_CoorsLight", 0.4,
    "VDK.VAC_Can_CoronaExtra", 0.4,
    "VDK.VAC_Can_Heineken",  0.6,
    "VDK.VAC_Can_MillerLite", 0.6,
    "VDK.VAC_Can_Modelo", 0.6,
    "VDK.VAC_Can_BlueRibbon", 0.6,
  }
else
  print("VDK Vacs Drinks | NOT using 1993 drinks only (Alcohol) | 1993=",enableImmersion)
  alcoholitemTable = {
    "VDK.VAC_Can_Budlight", 1,
    "VDK.VAC_Can_Budweiser", 0.6,
    "VDK.VAC_Can_CoorsLight", 0.4,
    "VDK.VAC_Can_CoronaExtra", 0.4,
    "VDK.VAC_Can_Heineken",  0.6,
    "VDK.VAC_Can_MillerLite", 0.6,
    "VDK.VAC_Can_Modelo", 0.6,
    "VDK.VAC_Can_BlueRibbon", 0.6,
    "VDK.VAC_Can_BlueMoon", 0.6,
    "VDK.VAC_Bottle_AbsolutVodkaFull", 0.1,
  }
end

for i = 1, #alcoholspawnTable, 2 do
  for j = 1, #alcoholitemTable, 2 do
    table.insert(ProceduralDistributions.list[alcoholspawnTable[i]].items, alcoholitemTable[j])
    table.insert(ProceduralDistributions.list[alcoholspawnTable[i]].items, alcoholspawnTable[i+1] * alcoholitemTable[j+1])
  end
end

local nonalcoholspawnTable = {
  "BandPracticeFridge", 0.3,
  "BarCounterMisc", 0.7,
  "CafeteriaDrinks", 0.6,
  "CrateSodaCans", 1.4,
  "CrateSodaCans", 0.7,
  "FridgeBreakRoom", 0.3,
  "FridgeOffice", 0.3,
  "FridgeSoda", 1.4,
  "FridgeSoda", 0.7,
  "FridgeTrailerPark", 0.1,
  "GigamartBottles", 1.4,
  "GigamartBottles", 0.7,
  "KitchenBottles", 0.7,
  "MotelFridge", 0.4,
  "StoreShelfCombo", 0.7,
  "StoreShelfDrinks", 1.4,
  "StoreShelfDrinks", 0.7,
  "TheatreDrinks", 0.7,
  "TheatreDrinks", 0.7,
  "ClassroomDesk", 0.1,
}

local nonalcoholitemTable = {}

if(enableImmersion)
then
  print("VDK Vacs Drinks | Using 1993 drinks only (NonAlcohol) | 1993=",enableImmersion)
  nonalcoholitemTable = {
    "VDK.VAC_Can_RedBull", 0.3,
    "VDK.VAC_Can_CocaCola", 1.0,
    "VDK.VAC_Can_Pepsi", 1.0,
    "VDK.VAC_Can_DietCoke", 0.6,
    "VDK.VAC_Can_DrPepper", 0.8,
    "VDK.VAC_Can_MountainDew", 0.8,
    "VDK.VAC_Can_Sprite", 0.8,
    "VDK.VAC_Can_DietPepsi", 0.6,
    "VDK.VAC_Can_Fanta", 0.8,
    "VDK.VAC_Can_MugRootBeer", 0.8,
  }
else
  print("VDK Vacs Drinks | NOT using 1993 drinks only (NonAlcohol) | 1993=",enableImmersion)
  nonalcoholitemTable = {
    "VDK.VAC_Can_Burn", 0.2,
    "VDK.VAC_Can_Monster", 0.3,
    "VDK.VAC_Can_Nos", 0.2,
    "VDK.VAC_Can_RedBull", 0.3,
    "VDK.VAC_Can_Rockstar", 0.2,
    "VDK.VAC_Can_CocaCola", 1.0,
    "VDK.VAC_Can_Pepsi", 1.0,
    "VDK.VAC_Can_DietCoke", 0.6,
    "VDK.VAC_Can_DrPepper", 0.8,
    "VDK.VAC_Can_MountainDew", 0.8,
    "VDK.VAC_Can_Sprite", 0.8,
    "VDK.VAC_Can_DietPepsi", 0.6,
    "VDK.VAC_Can_Fanta", 0.8,
    "VDK.VAC_Can_MugRootBeer", 0.8,
    "VDK.VAC_Can_5HourEnergyDrink", 0.1,
  }
end

for i = 1, #nonalcoholspawnTable, 2 do
  for j = 1, #nonalcoholitemTable, 2 do
    table.insert(ProceduralDistributions.list[nonalcoholspawnTable[i]].items, nonalcoholitemTable[j])
    table.insert(ProceduralDistributions.list[nonalcoholspawnTable[i]].items, nonalcoholspawnTable[i+1] * nonalcoholitemTable[j+1])
  end
end

local miscItemSpawnTable = {
  "BandPracticeFridge", 1.1,
  "BandPracticeFridge", 0.5,
  "BarCounterLiquor", 0.5,
  "BarCounterWeapon", 0.5,
  "BarShelfLiquor", 0.5,
  "BreweryCans", 0.5,
  "FridgeBeer", 1.1,
  "FridgeBeer", 0.5,
  "FridgeGeneric", 0.05,
  "FridgeTrailerPark", 0.2,
  "KitchenBottles", 0.1,
  "StoreShelfBeer", 0.5,
}

local miscItemTable = {
  "VDK.VAC_Can_LongDrink", 0.5,
  "VDK.VAC_Can_Karhu", 0.2,
}

if(spawnMisc)then
  print("VDK Vacs Drinks | SPAWNING MISC DRINKS. spawnMisc =",spawnMisc)
  for i = 1, #miscItemSpawnTable, 2 do
    for j = 1, #miscItemTable, 2 do
      table.insert(ProceduralDistributions.list[miscItemSpawnTable[i]].items, miscItemTable[j])
      table.insert(ProceduralDistributions.list[miscItemSpawnTable[i]].items, miscItemSpawnTable[i+1] * miscItemTable[j+1])
    end
  end
end

local removefromDistTable = {
  "BandPracticeFridge",
  "BarCounterLiquor",
  "BarCounterWeapon",
  "BarShelfLiquor",
  "BreweryCans",
  "FridgeBeer",
  "FridgeGeneric",
  "FridgeTrailerPark",
  "KitchenBottles",
  "StoreShelfBeer",
  "BarCounterMisc",
  "CafeteriaDrinks",
  "CrateSodaCans",
  "CrateSodaCans",
  "FridgeBreakRoom",
  "FridgeOffice",
  "FridgeSoda",
  "GigamartBottles",
  "MotelFridge",
  "StoreShelfCombo",
  "StoreShelfDrinks",
  "TheatreDrinks",
  "ClassroomDesk",
}

local removefromItemTable = {
  "BeerBottle",
  "BeerCan",
  "Pop",
  "Pop2",
  "Pop3",
  "PopBottle",
}

if(removePopSpawns)then
  print("VDK Vacs Drinks | Removing basegame spawns | RemovePopSpawns=",removePopSpawns)
  for i = 1, #removefromDistTable do
    for j = 1, #removefromItemTable do
      for k, v in ipairs(ProceduralDistributions.list[removefromDistTable[i]].items) do
        if(ProceduralDistributions.list[removefromDistTable[i]].items[k] == removefromItemTable[j])
        then
          table.remove(ProceduralDistributions.list[removefromDistTable[i]].items, k)
          table.remove(ProceduralDistributions.list[removefromDistTable[i]].items, k+1)
        end
      end
    end
  end
else
  print("VDK Vacs Drinks | NOT removing BaseGame spawns | RemovePopSpawns=",removePopSpawns)
end
