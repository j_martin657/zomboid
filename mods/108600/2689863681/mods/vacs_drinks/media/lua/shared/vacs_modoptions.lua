--Default options.
local OPTIONS = {
  box1 = false,
  box2 = true,
  box3 = false,
}

-- Connecting the options to the menu, so user can change them.
if ModOptions and ModOptions.getInstance then
  local settings = ModOptions:getInstance(OPTIONS, "VDK", "Vac's Drinks")

  settings.names = {
    box1 = "Only use pre-1993 drinks",
    box2 = "Remove base game drink spawns",
    box3 = "Spawn misc drinks",
  }
end

--Make a link
VACS_DRINKS_MO = {} -- global variable (pick another name!)
VACS_DRINKS_MO.OPTIONS = OPTIONS
