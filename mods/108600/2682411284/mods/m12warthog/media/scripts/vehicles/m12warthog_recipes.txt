module m12

{

	recipe Make M12 Left Door
	{
		keep WeldingMask,
		FrontWindow2=1,
		BlowTorch=15,
       	SmallSheetMetal=4,
		SheetMetal=2,
		Screws=2,

		Result:M12LeftDoor2,
		Time:1000.0,
		Category:M12,
		SkillRequired:MetalWelding=5,
		OnGiveXP:Recipe.OnGiveXP.MetalWelding20,
	}

	recipe Make M12 Right Door
	{
		keep WeldingMask,
		FrontWindow2=1,
		BlowTorch=15,
       	SmallSheetMetal=4,
		SheetMetal=2,
		Screws=2,

		Result:M12RightDoor2,
		Time:1000.0,
		Category:M12,
		SkillRequired:MetalWelding=5,
		OnGiveXP:Recipe.OnGiveXP.MetalWelding20,
	}

	recipe Convert Standard RunFlat Tire
	{
		V100Tire2=1,
		keep LugWrench,

		Result:M12Tire2,
		Time:500.0,
		Category:M12,
		SkillRequired:Mechanics=3,
	}

}