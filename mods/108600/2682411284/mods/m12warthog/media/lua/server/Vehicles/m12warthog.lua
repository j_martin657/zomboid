local distributionTable = VehicleDistributions[1]

VehicleDistributions.haloHealthPack = {
    rolls = 1,
    items = {
        "FirstAidKit", 500,
    }
}

VehicleDistributions.haloChiefHelmet = {
    rolls = 1,
    items = {
        "Base.Hat_MCHelmet", 25,
    }
}

VehicleDistributions.m12alpha = {

	m12warthogTrunk = VehicleDistributions.haloChiefHelmet;

	m12warthogGloveBox1 = VehicleDistributions.haloHealthPack;
	m12warthogGloveBox2 = VehicleDistributions.haloHealthPack;
}

distributionTable["m12warthog"] = { Normal = VehicleDistributions.m12alpha; }
distributionTable["m12police"] = { Normal = VehicleDistributions.m12alpha; }
distributionTable["m12transport"] = { Normal = VehicleDistributions.m12alpha; }
