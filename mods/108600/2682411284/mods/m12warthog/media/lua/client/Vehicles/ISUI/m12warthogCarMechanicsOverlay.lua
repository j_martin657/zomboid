local function info()

ISCarMechanicsOverlay.CarList["Base.m12warthog"] = {imgPrefix = "m12warthog_", x=10,y=0};
ISCarMechanicsOverlay.CarList["Base.m12police"] = {imgPrefix = "m12warthog_", x=10,y=0};
ISCarMechanicsOverlay.CarList["Base.m12transport"] = {imgPrefix = "m12warthog_", x=10,y=0};
--
ISCarMechanicsOverlay.PartList["Battery"].vehicles = ISCarMechanicsOverlay.PartList["Battery"].vehicles or {};
ISCarMechanicsOverlay.PartList["Battery"].vehicles["m12warthog_"] = {img="battery", x=225,y=225,x2=270,y2=260};
--
ISCarMechanicsOverlay.PartList["SuspensionFrontLeft"].vehicles = ISCarMechanicsOverlay.PartList["SuspensionFrontLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["SuspensionFrontLeft"].vehicles["m12warthog_"] = {img="suspension_front_left", x=14,y=133,x2=55,y2=170};
ISCarMechanicsOverlay.PartList["SuspensionFrontRight"].vehicles = ISCarMechanicsOverlay.PartList["SuspensionFrontRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["SuspensionFrontRight"].vehicles["m12warthog_"] = {img="suspension_front_right", x=228,y=133,x2=270,y2=170};
ISCarMechanicsOverlay.PartList["SuspensionRearLeft"].vehicles = ISCarMechanicsOverlay.PartList["SuspensionRearLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["SuspensionRearLeft"].vehicles["m12warthog_"] = {x=14,y=335,x2=55,y2=372};
ISCarMechanicsOverlay.PartList["SuspensionRearRight"].vehicles = ISCarMechanicsOverlay.PartList["SuspensionRearRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["SuspensionRearRight"].vehicles["m12warthog_"] = {x=228,y=335,x2=270,y2=372};
--
ISCarMechanicsOverlay.PartList["BrakeFrontLeft"].vehicles = ISCarMechanicsOverlay.PartList["BrakeFrontLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["BrakeFrontLeft"].vehicles["m12warthog_"] = {img="brake_front_left", x=14,y=170,x2=55,y2=208};
ISCarMechanicsOverlay.PartList["BrakeFrontRight"].vehicles = ISCarMechanicsOverlay.PartList["BrakeFrontRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["BrakeFrontRight"].vehicles["m12warthog_"] = {img="brake_front_right", x=228,y=170,x2=270,y2=208};
ISCarMechanicsOverlay.PartList["BrakeRearLeft"].vehicles = ISCarMechanicsOverlay.PartList["BrakeRearLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["BrakeRearLeft"].vehicles["m12warthog_"] = {x=14,y=372,x2=55,y2=410};
ISCarMechanicsOverlay.PartList["BrakeRearRight"].vehicles = ISCarMechanicsOverlay.PartList["BrakeRearRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["BrakeRearRight"].vehicles["m12warthog_"] = {x=228,y=372,x2=270,y2=410};
--
ISCarMechanicsOverlay.PartList["Engine"].vehicles = ISCarMechanicsOverlay.PartList["Engine"].vehicles or {};
ISCarMechanicsOverlay.PartList["Engine"].vehicles["m12warthog_"] = {x=91,y=35,x2=191,y2=96};
--
ISCarMechanicsOverlay.PartList["HeadlightLeft"].vehicles = ISCarMechanicsOverlay.PartList["HeadlightLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["HeadlightLeft"].vehicles["m12warthog_"] = {x=123,y=131,x2=138,y2=135};
ISCarMechanicsOverlay.PartList["HeadlightRight"].vehicles = ISCarMechanicsOverlay.PartList["HeadlightRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["HeadlightRight"].vehicles["m12warthog_"] = {x=145,y=131,x2=160,y2=135};
--
ISCarMechanicsOverlay.PartList["EngineDoor"].vehicles = ISCarMechanicsOverlay.PartList["EngineDoor"].vehicles or {};
ISCarMechanicsOverlay.PartList["EngineDoor"].vehicles["m12warthog_"] = {x=82,y=137,x2=201,y2=194};
--
ISCarMechanicsOverlay.PartList["Muffler"] = {img="muffler", vehicles = {"m12warthog_"}};
ISCarMechanicsOverlay.PartList["Muffler"].vehicles = ISCarMechanicsOverlay.PartList["Muffler"].vehicles or {};
ISCarMechanicsOverlay.PartList["Muffler"].vehicles["m12warthog_"] = {x=14,y=260,x2=50,y2=329};
--
ISCarMechanicsOverlay.PartList["SeatFrontLeft"] = {img="seat_left", vehicles = {"m12warthog_"}};
ISCarMechanicsOverlay.PartList["SeatFrontLeft"].vehicles = ISCarMechanicsOverlay.PartList["SeatFrontLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["SeatFrontLeft"].vehicles["m12warthog_"] = {x=99,y=253,x2=129,y2=297};
ISCarMechanicsOverlay.PartList["SeatFrontRight"] = {img="seat_right", vehicles = {"m12warthog_"}};
ISCarMechanicsOverlay.PartList["SeatFrontRight"].vehicles = ISCarMechanicsOverlay.PartList["SeatFrontRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["SeatFrontRight"].vehicles["m12warthog_"] = {x=155,y=253,x2=185,y2=297};
--
ISCarMechanicsOverlay.PartList["TireFrontLeft"].vehicles = ISCarMechanicsOverlay.PartList["TireFrontLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["TireFrontLeft"].vehicles["m12warthog_"] = {x=64,y=137,x2=82,y2=203};
ISCarMechanicsOverlay.PartList["TireFrontRight"].vehicles = ISCarMechanicsOverlay.PartList["TireFrontRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["TireFrontRight"].vehicles["m12warthog_"] = {x=201,y=137,x2=220,y2=203};
ISCarMechanicsOverlay.PartList["TireRearLeft"].vehicles = ISCarMechanicsOverlay.PartList["TireRearLeft"].vehicles or {};
ISCarMechanicsOverlay.PartList["TireRearLeft"].vehicles["m12warthog_"] = {x=64,y=340,x2=82,y2=404};
ISCarMechanicsOverlay.PartList["TireRearRight"].vehicles = ISCarMechanicsOverlay.PartList["TireRearRight"].vehicles or {};
ISCarMechanicsOverlay.PartList["TireRearRight"].vehicles["m12warthog_"] = {x=201,y=340,x2=220,y2=404};
--
ISCarMechanicsOverlay.PartList["Windshield"].vehicles = ISCarMechanicsOverlay.PartList["Windshield"].vehicles or {};
ISCarMechanicsOverlay.PartList["Windshield"].vehicles["m12warthog_"] = {x=97,y=194,x2=186,y2=236};
--
ISCarMechanicsOverlay.PartList["GasTank"].vehicles = ISCarMechanicsOverlay.PartList["GasTank"].vehicles or {};
ISCarMechanicsOverlay.PartList["GasTank"].vehicles["m12warthog_"] = {img="gastank", x=158,y=470,x2=246,y2=527};
--
--

end


Events.OnInitWorld.Add(info);