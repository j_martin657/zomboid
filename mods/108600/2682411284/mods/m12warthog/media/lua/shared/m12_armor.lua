--***********************************************************
--**                          KI5                          **
--***********************************************************

Events.OnPlayerUpdate.Add(function(player, vehicle, args, part)
    local vehicle = player.getVehicle and player:getVehicle() or nil
    if (vehicle and string.find( vehicle:getScriptName(), "m12" )) then

-- m12 RunFlats

	local part = vehicle:getPartById("TireFrontLeft")
    		if part:getCondition() < 49 then
				sendClientCommand(player, "vehicle", "setPartCondition", { vehicle = vehicle:getId(), part = part:getId(), condition = 49 })
				--print ("tireFL repaired")
	end

		local part = vehicle:getPartById("TireFrontLeft")
    		if part:getContainerContentAmount() < 35 then
				sendClientCommand(player, "vehicle", "setContainerContentAmount", { vehicle = vehicle:getId(), part = part:getId(), amount = 35 })
				--print ("tireFL pumped")
		end

	local part = vehicle:getPartById("TireFrontRight")
	    		if part:getCondition() < 49 then
					sendClientCommand(player, "vehicle", "setPartCondition", { vehicle = vehicle:getId(), part = part:getId(), condition = 49 })
				--print ("tireFR repaired")
		end

		local part = vehicle:getPartById("TireFrontRight")
    		if part:getContainerContentAmount() < 35 then
				sendClientCommand(player, "vehicle", "setContainerContentAmount", { vehicle = vehicle:getId(), part = part:getId(), amount = 35 })
				--print ("tireFR pumped")
		end

	local part = vehicle:getPartById("TireRearLeft")
    		if part:getCondition() < 49 then
				sendClientCommand(player, "vehicle", "setPartCondition", { vehicle = vehicle:getId(), part = part:getId(), condition = 49 })
				--print ("tireRL repaired")
	end

		local part = vehicle:getPartById("TireRearLeft")
    		if part:getContainerContentAmount() < 35 then
				sendClientCommand(player, "vehicle", "setContainerContentAmount", { vehicle = vehicle:getId(), part = part:getId(), amount = 35 })
				--print ("tireRL pumped")
		end

	local part = vehicle:getPartById("TireRearRight")
	    		if part:getCondition() < 49 then
					sendClientCommand(player, "vehicle", "setPartCondition", { vehicle = vehicle:getId(), part = part:getId(), condition = 49 })
				--print ("tireRR repaired")
		end

		local part = vehicle:getPartById("TireRearRight")
    		if part:getContainerContentAmount() < 35 then
				sendClientCommand(player, "vehicle", "setContainerContentAmount", { vehicle = vehicle:getId(), part = part:getId(), amount = 35 })
				--print ("tireRR pumped")
		end

	local part = vehicle:getPartById("GasTank")
		    		if part:getCondition() < 50 then
						sendClientCommand(player, "vehicle", "setPartCondition", { vehicle = vehicle:getId(), part = part:getId(), condition = 50 })
					--print ("GasTank repaired")
			end

	local part = vehicle:getPartById("Windshield")
		    		if part:getCondition() < 48 then
						sendClientCommand(player, "vehicle", "setPartCondition", { vehicle = vehicle:getId(), part = part:getId(), condition = 48 })
					--print ("Windshield repaired")
			end

end

end)