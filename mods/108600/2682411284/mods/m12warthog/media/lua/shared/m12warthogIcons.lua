ContainerButtonIcons = ContainerButtonIcons or {}

local t = {}
t.trunk = getTexture("media/textures/trunk_m12.png")
t.healthPack = getTexture("media/textures/M12medkit.png")

ContainerButtonIcons.m12warthogTrunk = t.trunk
ContainerButtonIcons.m12warthogGloveBox1 = t.healthPack
ContainerButtonIcons.m12warthogGloveBox2 = t.healthPack
