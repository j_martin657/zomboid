if VehicleZoneDistribution then

VehicleZoneDistribution.parkingstall.vehicles["Base.m12warthog"] = {index = -1, spawnChance = 1};
VehicleZoneDistribution.parkingstall.vehicles["Base.m12transport"] = {index = -1, spawnChance = 1};

VehicleZoneDistribution.trailerpark.vehicles["Base.m12warthog"] = {index = -1, spawnChance = 1};
VehicleZoneDistribution.trailerpark.vehicles["Base.m12Burnt"] = {index = -1, spawnChance = 2};

VehicleZoneDistribution.police.vehicles["Base.m12police"] = {index = -1, spawnChance = 2};

VehicleZoneDistribution.ranger.vehicles["Base.m12warthog"] = {index = -1, spawnChance = 1};

VehicleZoneDistribution.spiffo.vehicles["Base.m12transport"] = {index = -1, spawnChance = 1};

-- Trafficjam spawns --

VehicleZoneDistribution.trafficjams.vehicles["Base.m12warthog"] = {index = -1, spawnChance = 1};
VehicleZoneDistribution.trafficjams.vehicles["Base.m12Burnt"] = {index = -1, spawnChance = 1};

-- Military spawn --

VehicleZoneDistribution.military = VehicleZoneDistribution.military or {}
VehicleZoneDistribution.military.vehicles = VehicleZoneDistribution.military.vehicles or {}

VehicleZoneDistribution.military.vehicles["Base.m12warthog"] = {index = -1, spawnChance = 5};

VehicleZoneDistribution.military.vehicles["Base.m12transport"] = {index = -1, spawnChance = 5};
end