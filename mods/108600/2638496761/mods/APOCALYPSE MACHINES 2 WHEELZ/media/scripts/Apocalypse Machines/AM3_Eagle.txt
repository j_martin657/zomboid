module Base
{

	model AME_Eagle
	{
		mesh = vehicles/AME_Eagle,
		shader = vehicle_multiuv,
		invertX = FALSE,
		scale = 0.0045,
	}
	model AME_Eagle_seat
	{
		mesh = vehicles/AME_Eagle_seat,
		shader = vehicle_multiuv,
		invertX = FALSE,
		scale = 0.0045,
	}
	/*	model AME_mobilityscooter
	{
		mesh = vehicles/AMI_mobilityscooter,
		shader = vehicle_multiuv,
		invertX = FALSE,
		scale = 0.0045,
	}
	model AME_mobilityscooter_seat
	{
		mesh = vehicles/AME_Eagle_seat,
		shader = vehicle_multiuv,
		invertX = FALSE,
		scale = 0.0045,
	}*/

	vehicle AME_Eagle
	{
		mechanicType = 5,		
		offRoadEfficiency = 0.8,
		engineRepairLevel = 3,
		playerDamageProtection = 0.8,
		spawnOffsetY = 0.19402517,

		model
		{
			file = AME_Eagle,
			scale = 1.8200,
			offset = 0.0000 0.3022 0.0000,
			rotate = 0 0 0,
		}

		skin
		{
			texture = Vehicles/AME_Eagle,
		}

		skin
		{
			texture = Vehicles/AME_Eagle_Red,
		}

		skin
		{
			texture = Vehicles/AME_Eagle_Purple,
		}

		textureMask = Vehicles/Blank,
		textureLights = Vehicles/Blank,
		textureDamage1Overlay = Vehicles/Blank,
		textureDamage2Overlay = Vehicles/Blank,
		textureDamage1Shell = Vehicles/Blank,
		textureDamage2Shell = Vehicles/Blank,
		textureRust = Vehicles/Blank,

		sound
		{
			engine = StoveRunning,
			engineStart = RadioButton,
			engineTurnOff = RadioButton,
			horn = VehicleHornStandard,
			ignitionFail = RadioButton,
		}

		extents = 0.3846 0.4176 0.7692,
		mass = 250,
		physicsChassisShape = 0.3626 0.4176 0.7692,
		centerOfMassOffset = 0.0000 0.3077 0.0000,
		shadowExtents = 0.3846 0.7692,
		shadowOffset = 0.0000 0.0000,
		engineForce = 1200,
		engineQuality = 60,
		engineLoudness = 35,
		maxSpeed = 5f,
		brakingForce = 60,
		stoppingMovementForce = 0.25f,
		rollInfluence = 1.0f,
		steeringIncrement = 0.03,
		steeringClamp = 0.3,
		suspensionStiffness = 30,
		suspensionCompression = 2.83,
		suspensionDamping = 2.88,
		maxSuspensionTravelCm = 10,
		suspensionRestLength = 0.2,
		wheelFriction = 1.6f,
		frontEndHealth = 150,
		rearEndHealth = 150,
		seats = 1,
		template = PassengerSeat2,

		passenger FrontLeft
		{
			vehicle.passenger.showPassenger=true,
			showPassenger=true,
			hasRoof = false,

			position inside
			{
				offset = 0.0000 0.0714 -0.1978,
				rotate = 0.0000 0.0000 0.0000,
			}

			position outside
			{
				offset = 0.5714 -0.4615 -0.0659,
				rotate = 0.0000 0.0000 0.0000,
				area = SeatFrontLeft,
				offset = 0.0000 0.0000 0.0000,
			}
			anim idle
			{
				anim = Idle,
				rate = 1.0,
			}
		}

		area SeatFrontLeft
		{
			xywh = 0.4341 -0.0879 0.4725 0.4725,
		}

		wheel FrontLeft
		{
			front = true,
			offset = 0.1264 -0.2747 0.2527,
			radius = 0.075f,
			width = 0.2f,
		}

		wheel FrontRight
		{
			front = true,
			offset = -0.1264 -0.2747 0.2527,
			radius = 0.075f,
			width = 0.2f,
		}

		wheel RearLeft
		{
			front = false,
			offset = 0.1593 -0.2747 -0.2692,
			radius = 0.075f,
			width = 0.2f,
		}

		wheel RearRight
		{
			front = false,
			offset = -0.1593 -0.2747 -0.2692,
			radius = 0.075f,
			width = 0.2f,
		}

		area Engine
		{
			xywh = 0.0000 0.6264 0.8352 0.4725,
		}

		area TruckBed
		{
			xywh = 0.0000 0.2582 0.8352 0.4725,
		}

		area TireFrontLeft
		{
			xywh = 0.4341 0.2527 0.4725 0.4725,
		}

		area TireFrontRight
		{
			xywh = -0.4286 0.2692 0.4725 0.4725,
		}

		area TireRearLeft
		{
			xywh = 0.4286 -0.2857 0.4725 0.4725,
		}

		area TireRearRight
		{
			xywh = -0.4286 -0.2802 0.4725 0.4725,
		}

		template = Trunk/part/TruckBed,

		part TruckBed
		{
			itemType = Base.SmallTrunk,

			container
			{
				capacity = 10,
				test = Vehicles.ContainerAccess.AME_TruckBedOpen,
			}
		}

		template = Seat/part/SeatFrontLeft,

		part SeatFrontLeft
		{	model default
			{
				file = AME_Eagle_seat,
			}
			specificItem = false,
			itemType = Base.NormalCarSeat1,
			lua
			{
				create = Vehicles.Create.Default,
				init = Vehicles.AM2_Part,
				update = Vehicles.AM2_Part,
			}
		}

		template = GasTank,

		part GasTank
		{
			category = nodisplay,
			specificItem = false,
			itemType = filcher.SmallCycleGasTank,

			lua
			{
				create = Vehicles.Create.GasTank,
				update = Vehicles.Update.AME_GasTank,
				checkEngine = Vehicles.CheckEngine.AME_GasTank,
			}
		}

		template = Battery,

		part Battery
		{
			specificItem = false,
			itemType = filcher.CycleBattery,

			lua
			{
				create = Vehicles.Create.Battery,
				update = Vehicles.Update.AME_Battery,
			}
		}

		template = Engine,

		part PassengerCompartment
		{
			category = nodisplay,

			lua
			{
				update = Vehicles.Update.PassengerCompartment,
			}
		}

		template = Window/part/WindowFrontLeft,
		template = Window/part/WindowFrontRight,

		part Window*
		{
			category = nodisplay,
			itemType= Base.nil,
		}

		template = Door/part/DoorFrontLeft,
		template = Door/part/DoorFrontRight,

		part Door*
		{
			category = nodisplay,
			itemType= Base.nil,
		}

		template = Tire,

		part TireFrontLeft
		{
			specificItem = false,
			itemType = Base.OldScooterTire;Base.NormalScooterTire;Base.ModernScooterTire,

			model InflatedTirePlusWheel
			{
				file = AM2_Vesper_Wheel,
				scale = 0.425,
			}
		}

		part TireFrontRight
		{
			specificItem = false,
			itemType = Base.OldScooterTire;Base.NormalScooterTire;Base.ModernScooterTire,

			model InflatedTirePlusWheel
			{
				file = AM2_Vesper_Wheel,
				scale = 0.425,
			}
		}

		part TireRearLeft
		{
			specificItem = false,
			itemType = Base.OldScooterTire;Base.NormalScooterTire;Base.ModernScooterTire,

			model InflatedTirePlusWheel
			{
				file = AM2_Vesper_Wheel,
				scale = 0.425,
			}
		}

		part TireRearRight
		{
			specificItem = false,
			itemType = Base.OldScooterTire;Base.NormalScooterTire;Base.ModernScooterTire,

			model InflatedTirePlusWheel
			{
				file = AM2_Vesper_Wheel,
				scale = 0.425,
			}
		}

		template = Brake,
		template = Suspension,

		part Brake*
		{
			specificItem = false,
			itemType = Base.OldScooterBrake;Base.NormalScooterBrake;Base.ModernScooterBrake,

			table install
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}

			table uninstall
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}
		}

		part Suspension*
		{
			specificItem = false,
			itemType = Base.NormalScooterSuspension;Base.ModernScooterSuspension,

			table install
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}

			table uninstall
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}
		}

		/*template = Muffler,

		part Muffler
		{
			specificItem = false,
			itemType = Base.OldScooterMuffler;Base.NormalScooterMuffler;Base.ModernScooterMuffler,

			table install
			{
				recipes = Motorcycle Mechanics,
			}

			table uninstall
			{
				recipes = Motorcycle Mechanics,
			}
		}*/

	}
}
