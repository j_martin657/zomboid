module Base
{
	model AM2_Vesper
	{
		mesh = vehicles/AM2_Vesper,
		texture = vehicles/AM2_Vesper,
		shader = vehicle_norandom_multiuv,
		static = TRUE,
		invertX = FALSE,
		scale = 0.01125,
	}
	model AM2_Vesper_Wheel
	{
		mesh = vehicles/AM2_Vesper_Wheel,
		texture = vehicles/AM2_Vesper,
		shader = vehiclewheel,
		scale = 0.01125,
	}
	vehicle AM2_Vesper
	{
		mechanicType = 5,
		engineRepairLevel = 3,
		playerDamageProtection = 1.25,
		offRoadEfficiency = 1.4,

		model
		{
			file = AM2_Vesper,
			scale = 0.9000,
			offset = 0.0000 0.0333 -0.0778,
			rotate = 0 0 0,
		}

		skin
		{
			texture = Vehicles/AM2_Vesper,
		}

		skin
		{
			texture = Vehicles/AM2_Vesper_Yellow,
		}

		skin
		{
			texture = Vehicles/AM2_Vesper_Green,
		}

		textureMask = Vehicles/AM2_Vesper_mask,
		textureLights = Vehicles/AM2_Vesper_lights,
		textureDamage1Overlay = Vehicles/Blank,
		textureDamage2Overlay = Vehicles/Blank,
		textureDamage1Shell = Vehicles/Blank,
		textureDamage2Shell = Vehicles/Blank,
		textureRust = Vehicles/Blank,

		sound
		{
			engine = VehicleEngineSmallCar,
			engineStart = VehicleEngineSmallCar,
			engineTurnOff = VehicleEngineSmallCar,
			horn = VehicleHornStandard,
			ignitionFail = VehicleIgnitionFailSmallCar,
		}

		extents = 0.3556 0.8000 1.6889,
		mass = 150,
		physicsChassisShape = 0.3556 0.8000 1.6889,
		centerOfMassOffset = 0.0000 0.3889 -0.0222,
		shadowExtents = 0.3622 1.6867,
		shadowOffset = 0.0000 -0.0200,
		engineForce = 3600,
		engineQuality = 80,
		engineLoudness = 45,
		maxSpeed = 50f,
		gearRatioCount = 5,
		gearRatioR = 3.5,
		gearRatio1 = 4.11,
		gearRatio2 = 2.50,
		gearRatio3 = 1.65,
		gearRatio4 = 1.25,
		gearRatio5 = 0.80,
		gearRatio5 = 0.60,
		stoppingMovementForce = 0.25f,
		rollInfluence = 1.0f,
		steeringIncrement = 0.03,
		steeringClamp = 0.3,
		suspensionStiffness = 50,
		suspensionCompression = 4.1,
		suspensionDamping = 3.4,
		maxSuspensionTravelCm = 20,
		suspensionRestLength = 0.2f,
		wheelFriction = 1.6f,
		frontEndHealth = 150,
		rearEndHealth = 150,
		seats = 2,

		wheel FrontLeft
		{
			front = true,
			offset = 0.0000 -0.0111 0.6889,
			radius = 0.144f,
			width = 0.575f,
		}

		wheel RearLeft
		{
			front = false,
			offset = 0.0000 -0.0111 -0.5444,
			radius = 0.144f,
			width = 0.575f,
		}

		template = PassengerSeat2,

		passenger FrontLeft
		{
			showPassenger = true,
			hasRoof = false,

			position inside
			{
				offset = 0.0000 0.6333 -0.2556,
				rotate = 0.0000 0.0000 0.0000,
			}

			position outside
			{
				offset = 0.2444 -0.2333 0.2111,
				rotate = 0.0000 0.0000 0.0000,
				area = SeatFrontLeft,
				offset = 0.0000 0.0000 0.0000,
			}
		}

		passenger FrontRight
		{
			showPassenger = true,
			hasRoof = false,

			position inside
			{
				offset = 0.0000 0.7444 -0.4333,
				rotate = 0.0000 0.0000 0.0000,
			}

			position outside
			{
				offset = -0.5556 -0.3000 -0.1444,
				rotate = 0.0000 0.0000 0.0000,
				area = SeatFrontRight,
				offset = 0.0000 0.0000 -0.1800,
			}
		}

		area Engine
		{
			xywh = 0.0111 1.4333 1.6000 1.2000,
		}

		area SeatFrontLeft
		{
			xywh = 0.5111 -0.3000 0.6000 0.8000,
		}

		area SeatFrontRight
		{
			xywh = 0.0111 -1.3222 1.6000 0.9111,
		}

		area GasTank
		{
			xywh = 0.0111 -0.3444 1.6000 0.9111,
		}

		area TireFrontLeft
		{
			xywh = 0.0000 0.7889 1.7333 0.4667,
		}

		area TireRearLeft
		{
			xywh = 0.0000 -0.7778 1.7333 0.4667,
		}

		template = Seat/part/SeatFrontLeft,
		template = Seat/part/SeatFrontRight,

		part Seat*
		{
			category = nodisplay,
			specificItem = false,
			itemType = filcher.CycleSeatNoStorage,

			table install
			{
				skills = Mechanics:2,
				recipes = Motorcycle Mechanics,
			}

			table uninstall
			{
				skills = Mechanics:2,
				recipes = Motorcycle Mechanics,
			}
		}

		template = GasTank,

		part GasTank
		{
			specificItem = false,
			itemType = filcher.SmallCycleGasTank,

			install
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}

			uninstall
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}
		}

		template = Battery,

		part Battery
		{
			specificItem = false,
			itemType = filcher.CycleBattery,
		}

		template = Engine,
		template = Muffler,

		part Muffler
		{
			specificItem = false,
			itemType = Base.OldScooterMuffler;Base.NormalScooterMuffler;Base.ModernScooterMuffler,

			table install
			{
				recipes = Motorcycle Mechanics,
			}

			table uninstall
			{
				recipes = Motorcycle Mechanics,
			}
		}

		part PassengerCompartment
		{
			category = nodisplay,

			lua
			{
				update = Vehicles.Update.PassengerCompartment,
			}
		}

		template = Window/part/WindowFrontLeft,
		template = Window/part/WindowFrontRight,

		part Wind*
		{
			category = nodisplay,
			itemType= Base.nil,

			table install
			{
				recipes = Impossible,
			}

			table uninstall
			{
				recipes = Impossible,
			}
		}

		template = Door/part/DoorFrontLeft,
		template = Door/part/DoorFrontRight,

		part Door*
		{
			category = nodisplay,
			itemType= Base.nil,

			table install
			{
				sound = VehicleDoorOpenSportsCar,
			}

			anim Close
			{
				sound = VehicleDoorCloseSportsCar,
			}

			anim Lock
			{
				sound = LockVehicleDoorSportsCar,
			}

			anim Unlock
			{
				sound = UnlockVehicleDoorSportsCar,
			}

			anim IsLocked
			{
				sound = VehicleDoorIsLockedSportsCar,
			}

			table install
			{
				recipes = Advanced Mechanics,
			}

			table uninstall
			{
				recipes = Advanced Mechanics,
			}
		}

		template = Tire/part/TireFrontLeft,
		template = Tire/part/TireRearLeft,

		part TireFrontLeft
		{
			specificItem = false,
			itemType = Base.OldScooterTire;Base.NormalScooterTire;Base.ModernScooterTire,

			model InflatedTirePlusWheel
			{
				file = AM2_Vesper_Wheel,
				offset = 0.0000 0.0000 0.0000,
			}
		}

		part TireRearLeft
		{
			specificItem = false,
			itemType = Base.OldScooterTire;Base.NormalScooterTire;Base.ModernScooterTire,

			model InflatedTirePlusWheel
			{
				file = AM2_Vesper_Wheel,
			}
		}

		template = Brake/part/BrakeFrontLeft,
		template = Brake/part/BrakeRearLeft,

		part Brake*
		{
			specificItem = false,
			itemType = Base.OldScooterBrake;Base.NormalScooterBrake;Base.ModernScooterBrake,

			table install
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}

			table uninstall
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}
		}

		template = Suspension/part/SuspensionFrontLeft,
		template = Suspension/part/SuspensionRearLeft,

		part Suspension*
		{
			specificItem = false,
			itemType = Base.NormalScooterSuspension;Base.ModernScooterSuspension,

			table install
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}

			table uninstall
			{
				skills = Mechanics:3,
				recipes = Motorcycle Mechanics,
			}
		}

		template = Headlight/part/HeadlightLeft,


		part HeadlightLeft
		{
			lua
			{
				create = Vehicles.Create.AM2_Headlight,
				init = Vehicles.AM2_Part,
				update = Vehicles.AM2_Headlight,
			}
		}
		template = Headlight/part/HeadlightRight,


		part HeadlightRight
		{
			lua
			{
				create = Vehicles.Create.AM2_Headlight,
				init = Vehicles.AM2_Part,
				update = Vehicles.AM2_Headlight,
			}
		}
		template = Headlight/part/HeadlightRearLeft,
	}
}
