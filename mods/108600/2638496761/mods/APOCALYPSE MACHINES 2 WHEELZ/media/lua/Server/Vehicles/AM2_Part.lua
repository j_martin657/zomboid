require "Vehicle/Vehicles"


function Vehicles.AM2_Part(vehicle, part)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
end
function Vehicles.AM2_Part_Always(vehicle, part)
	part:setModelVisible("Default", true)
	part:setModelVisible("default", true)
end

function Vehicles.Update.AM2_GasTank(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.GasTank(vehicle, part, elapsedMinutes)
end

function Vehicles.Update.AM2_Battery(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.Battery(vehicle, part, elapsedMinutes)
end

function Vehicles.Update.AM2_Headlight(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.Headlight(vehicle, part, elapsedMinutes)
end

function Vehicles.Update.AM2_Radio(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.Radio(vehicle, part, elapsedMinutes)
end

function Vehicles.Update.AM2_Brakes(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.Brakes(vehicle, part, elapsedMinutes)
end

function Vehicles.Update.AM2_Suspension(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.Suspension(vehicle, part, elapsedMinutes)
end

function Vehicles.Update.AM2_Muffler(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.Muffler(vehicle, part, elapsedMinutes)
end

function Vehicles.Update.AM2_TrunkDoor(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.TrunkDoor(vehicle, part, elapsedMinutes)
end
function Vehicles.Update.AM2_Radio(vehicle, part, elapsedMinutes)
	if part:getItemType():isEmpty() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	elseif not part:getInventoryItem() then
		part:setModelVisible("Default", false)
		part:setModelVisible("default", false)
	else
		part:setModelVisible("Default", true)
		part:setModelVisible("default", true)
	end
	Vehicles.Update.Radio(vehicle, part, elapsedMinutes)
end
function Vehicles.AM2_Headlight(vehicle, part)
	local item = VehicleUtils.createPartInventoryItem(part)
	-- xOffset,yOffset,distance,intensity,dot,focusing
	-- NOTE: distance,intensity,focusing values are ignored, instead they are
	-- set based on part condition.
	if part:getId() == "HeadlightLeft" then
		part:createSpotLight(0, 2.0, 8.0+ZombRand(16.0), 0.75, 0.96, ZombRand(200))
	end
end