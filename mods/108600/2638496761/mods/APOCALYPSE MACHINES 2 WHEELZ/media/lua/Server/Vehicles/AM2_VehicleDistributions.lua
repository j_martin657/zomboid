require "Vehicles/VehicleDistributions"


VehicleDistributions.Patriot_GloveBox = {
	rolls = 1,
	items = {
		"Cigarettes", 7,
        "MintCandy", 10,
        "Lollipop", 10,
        "Candycane", 10,
		"Pills", 5,
		"PillsBeta", 5,
		"PillsAntiDep", 5,
        "Radio.CDplayer", 5,
        "Headphones", 5,
        "Sheet", 5,
        "Pillow", 5,
        "ToyBear", 5,
        "Longcoat_Bathrobe", 5,
        "HospitalGown", 5,
        "Glasses_Normal", 5,
        "Glasses_Reading", 5,
        "Antiobiotics", 5,
	},
	junk = {
		rolls = 1,
		items = {
            "Notebook", 10,
            "Magazine", 7,
            "Newspaper", 7,
			"Pen", 5,
            "BluePen", 5,
            "RedPen", 5,
            "Pencil", 5,
            "Eraser", 5,
            "RubberBand", 5,
			"Tissue", 5,
            "Tissue", 3,
			"Lighter", 5,
			"Matches", 7,
			"Lipstick", 3,
            -- "Cologne", 3,
            -- "Perfume", 3,
            -- "MakeupEyeshadow", 3,
            -- "MakeupFoundation", 3,
			"Comb", 5,
            "CreditCard", 5,
			"Wallet", 5,
			"Wallet2", 5,
			"Wallet3", 5,
			"Wallet4", 5,
			"ToiletPaper", 3,
			"Disc_Retail", 5,
            "Cockroach", 1,
		}
	}
}


VehicleDistributions.Patriot = {
	
	GloveBox = VehicleDistributions.Patriot_GloveBox
}


VehicleDistributions.Eagle_Trunk = {
	rolls = 1,
	items = {
		"Cigarettes", 7,
        "WhiskeyFull", 0.5,
        "MintCandy", 10,
        "Lollipop", 10,
        "Candycane", 10,
        "BeefJerky", 7,
		"Pills", 5,
		"PillsBeta", 5,
		"PillsAntiDep", 5,
		"Bandaid", 10,
        "Bandaid", 7,
		"Radio.WalkieTalkie1", 5,
        "Radio.CDplayer", 5,
        "Headphones", 5,
		"Pistol", 0.25,
        "Pistol2", 0.25,
        "Revolver_Short", 0.1,
        "HuntingKnife", 0.25,
		"Glasses_Sun", 0.25,
        "Glasses_Aviators", 0.1,
        "Base.MuldraughMap", 5,
        "Base.WestpointMap", 5,
        "Base.MarchRidgeMap", 5,
        "Base.RosewoodMap", 5,
        "Base.RiversideMap", 5,
        "Pillow", 5,
        "ToyBear", 5,
        "Glasses_Normal", 5,
        "Glasses_Reading", 5,
        "Antiobiotics", 5,
        "VideoGame", 5,
        "CordlessPhone", 5,
		"Crisps",5,
		"Crisps2", 5,
		"Crisps3", 5,
		"Crisps4", 5,
		"Cereal", 5,
		"Dogfood", 5,
		"CannedSardines", 2,
		"Purse", 7,
		"PillsSleepingTablets", 8,
		"PillsVitamins", 8,
		"DeadSquirrel", 1,
        "Hat_BucketHat", 5,
        "Hat_Visor_WhiteTINT", 5,
        "Shoes_FlipFlop", 5,
        "Shoes_Slippers", 5,
        "Bag_FannyPackFront", 5,
        "VHS_Retail", 5,
        "ComicBook", 5,
        "Yoyo", 5,
        "Cube", 5,
        "DogChew", 5,
        "Leash", 5,
        "PopBottle", 5,
        "Pop", 5,
        "Pop2", 5,
        "Pop3", 5,
        "BeerBottle", 5,
        "BeerCan", 5,
	},
	junk = {
		rolls = 1,
		items = {
            "Notebook", 10,
            "Magazine", 7,
            "Newspaper", 7,
			"Pen", 5,
            "BluePen", 5,
            "RedPen", 5,
            "Pencil", 5,
            "Eraser", 5,
            "RubberBand", 5,
			"Tissue", 5,
            "Tissue", 3,
			"Lighter", 5,
			"Matches", 7,
			"Lipstick", 3,
            "Cologne", 3,
            "Perfume", 3,
            "MakeupEyeshadow", 3,
            "MakeupFoundation", 3,
			"Comb", 5,
            "CreditCard", 5,
			"Wallet", 5,
			"Wallet2", 5,
			"Wallet3", 5,
			"Wallet4", 5,
			"ToiletPaper", 3,
			"Mirror", 3,
			"Disc_Retail", 5,
            "Cockroach", 1,
            "Garbagebag", 7,
			"Plasticbag", 7,
            "Plasticbag", 7,
            "Tote", 7,
            "Tissue", 7,
            "ToiletPaper", 7,
            "RubberBand", 7,
            "WaterBottleEmpty", 5,
            "PopBottleEmpty", 5,
            "PopEmpty", 5,
            "WhiskeyEmpty", 1,
		}
	}
}





VehicleDistributions.Eagle = {
	
	TruckBed = VehicleDistributions.Eagle_Trunk,
	TruckBedOpen = VehicleDistributions.Eagle_Trunk
}




VehicleDistributions.Courier_Trunk = {
	rolls = 1,
	items = {
		"Hat_CrashHelmet", 5,
		"Hat_CrashHelmetFULL", 5,
        "LugWrench", 20,
        "TirePump", 20,
        "NormalCycleTire", 20,
        "Jack", 20,
        "Wrench", 20,
        "Screwdriver", 20,
        "CycleBattery", 20,
		"EmptyPetrolCan", 20,
        "FirstAidKit", 5,
        "DuctTape", 3,
        "DuctTape", 3,
        "Twine", 3,
        "Tarp", 3,
        "EmptySandbag", 3,
        "LeadPipe", 1,
        "MetalPipe", 1,
        "PipeWrench", 1,
		"BaseballBat", 1,
		"Cigarettes", 7,
        "WhiskeyFull", 0.5,
        "Torch", 5,
        "Battery", 7,
        "BeefJerky", 7,
		"Pills", 5,
		"Bandaid", 10,
        "Bandaid", 7,
        "AlcoholWipes", 7,
        "Bandage", 5,
        "Twine", 5,
		"Radio.WalkieTalkie2", 5,
        "Radio.CDplayer", 5,
        "Headphones", 5,
		"Pistol", 0.25,
        "Pistol2", 0.25,
        "Revolver_Short", 0.1,
        "HuntingKnife", 0.25,
		"Glasses_Sun", 0.25,
        "Glasses_Aviators", 0.1,
        "Gloves_LeatherGlovesBlack", 0.1,
        "Gloves_LeatherGloves", 0.25,
        "Base.MuldraughMap", 5,
        "Base.WestpointMap", 5,
        "Base.MarchRidgeMap", 5,
        "Base.RosewoodMap", 5,
        "Base.RiversideMap", 5,
		"TW.Chain", 2,
	},
	junk = {
		rolls = 1,
		items = {
            "Garbagebag", 7,
			"Plasticbag", 7,
            "Plasticbag", 7,
            "Tissue", 7,
            "ToiletPaper", 7,
            "RubberBand", 7,
            "WaterBottleEmpty", 5,
            "PopBottleEmpty", 5,
            "PopEmpty", 5,
            "WhiskeyEmpty", 1,
            "Cockroach", 1,
            "CreditCard", 5,
			"Wallet", 5,
			"Wallet2", 5,
			"Wallet3", 5,
			"Wallet4", 5,
		}
	}
}



VehicleDistributions.Courier = {
	
	TruckBed = VehicleDistributions.Courier_Trunk,
	TruckBedOpen = VehicleDistributions.Courier_Trunk,
	GloveBox = VehicleDistributions.Courier_Trunk,
}

VehicleDistributions.Sidecar = {
	
	TruckBed = VehicleDistributions.Courier_Trunk,
	TruckBedOpen = VehicleDistributions.Courier_Trunk,
	GloveBox = VehicleDistributions.Courier_Trunk,
	SeatFrontRight = VehicleDistributions.Courier_Trunk,
}


VehicleDistributions.Commodore_GloveBox = {
	rolls = 1,
	items = {
		"Cigarettes", 7,
        "WhiskeyFull", 0.5,
        "BeefJerky", 7,
		"Pills", 5,
		"PillsBeta", 5,
		"PillsAntiDep", 5,
		"Radio.WalkieTalkie3", 5,
		"Pistol", 0.25,
        "Pistol2", 0.25,
        "Revolver_Short", 0.1,
		"Glasses_Sun", 5,
        "Glasses_Aviators", 5,
        "Glasses_Normal", 5,
        "Glasses_Reading", 5,
        "Antiobiotics", 5,
        "CordlessPhone", 5,
        "Hat_Visor_WhiteTINT", 5,
        "Hat_GolfHat", 5,
        "Gloves_LeatherGlovesBlack", 5,
        "Gloves_LeatherGloves", 5,
        "Pop", 5,
        "Pop2", 5,
        "Pop3", 5,
        "BeerBottle", 5,
        "BeerCan", 5,
		"GolfBall", 10,
		"GolfBall", 10,
		"GolfBall", 10,
		"GolfBall", 10,
	},
	junk = {
		rolls = 1,
		items = {
            "Notebook", 10,
            "Magazine", 7,
            "Newspaper", 7,
			"Pen", 5,
            "BluePen", 5,
            "RedPen", 5,
            "Pencil", 5,
            "Eraser", 5,
            "Tissue", 3,
			"Lighter", 5,
			"Matches", 7,
            "Cologne", 3,
			"Comb", 5,
            "CreditCard", 5,
			"Wallet", 5,
			"Wallet2", 5,
			"Wallet3", 5,
			"Wallet4", 5,
            "WhiskeyEmpty", 1,
		}
	}
}


VehicleDistributions.Commodore_Trunk = {
	rolls = 2,
	items = {
		"Cigarettes", 7,
        "WhiskeyFull", 0.5,
        "BeefJerky", 7,
		"Radio.WalkieTalkie3", 5,
		"Pistol", 0.25,
        "Pistol2", 0.25,
        "Revolver_Short", 0.1,
		"Glasses_Sun", 5,
        "Glasses_Aviators", 5,
        "Antiobiotics", 5,
        "Hat_Visor_WhiteTINT", 5,
        "Hat_GolfHat", 5,
        "Gloves_LeatherGlovesBlack", 5,
        "Gloves_LeatherGloves", 5,

        "Pop", 5,
        "Pop2", 5,
        "Pop3", 5,
        "BeerBottle", 5,
        "BeerCan", 5,
		"Golfclub", 10,
		"Golfclub", 3,
		"GolfBall", 10,
		"GolfBall", 10,
		"GolfBall", 10,
		"GolfBall", 10,
		"Bag_GolfBag", 10,
	},
	junk = {
		rolls = 1,
		items = {
            "Notebook", 10,
			"Pen", 5,
            "BluePen", 5,
            "RedPen", 5,
            "Pencil", 5,
            "Eraser", 5,
            "WhiskeyEmpty", 1,
		}
	}
}


VehicleDistributions.Commodore = {
	
	TruckBed = VehicleDistributions.Commodore_Trunk,
	TruckBedOpen = VehicleDistributions.Commodore_Trunk,
	GloveBox = VehicleDistributions.Commodore_GloveBox 
}



VehicleDistributions.Quad_Trunk = {
	rolls = 1,
	items = {
		"Hat_CrashHelmet", 5,
		"Hat_CrashHelmetFULL", 5,
        "LugWrench", 20,
        "TirePump", 20,
        "NormalATVTire", 20,
        "Jack", 20,
        "Wrench", 20,
        "Screwdriver", 20,
        "CycleBattery", 20,
		"EmptyPetrolCan", 20,
        "FirstAidKit", 5,
        "DuctTape", 3,
        "DuctTape", 3,
        "Twine", 3,
        "Tarp", 3,
        "EmptySandbag", 3,
        "LeadPipe", 1,
        "MetalPipe", 1,
        "PipeWrench", 1,
		"BaseballBat", 1,
		"Cigarettes", 7,
        "WhiskeyFull", 0.5,
        "Torch", 5,
        "Battery", 7,
        "BeefJerky", 7,
		"Pills", 5,
		"Bandaid", 10,
        "Bandaid", 7,
        "AlcoholWipes", 7,
        "Bandage", 5,
        "Twine", 5,
		"Radio.WalkieTalkie2", 5,
        "Radio.CDplayer", 5,
        "Headphones", 5,
		"Pistol", 0.25,
        "Pistol2", 0.25,
        "Revolver_Short", 0.1,
        "HuntingKnife", 0.25,
		"Glasses_Sun", 0.25,
        "Glasses_Aviators", 0.1,
        "Gloves_LeatherGlovesBlack", 0.1,
        "Gloves_LeatherGloves", 0.25,
        "Base.MuldraughMap", 5,
        "Base.WestpointMap", 5,
        "Base.MarchRidgeMap", 5,
        "Base.RosewoodMap", 5,
        "Base.RiversideMap", 5,
		"TW.Chain", 2,
	},
	junk = {
		rolls = 1,
		items = {
            "Garbagebag", 7,
			"Plasticbag", 7,
            "Plasticbag", 7,
            "Tissue", 7,
            "ToiletPaper", 7,
            "RubberBand", 7,
            "WaterBottleEmpty", 5,
            "PopBottleEmpty", 5,
            "PopEmpty", 5,
            "WhiskeyEmpty", 1,
            "Cockroach", 1,
            "CreditCard", 5,
			"Wallet", 5,
			"Wallet2", 5,
			"Wallet3", 5,
			"Wallet4", 5,
		}
	}
}



VehicleDistributions.Quad = {
	
	TruckBed = VehicleDistributions.Quad_Trunk,
	TruckBed1 = VehicleDistributions.Quad_Trunk,
	TruckBed2 = VehicleDistributions.Quad_Trunk,
	TruckBedOpen = VehicleDistributions.Quad_Trunk,
	TruckBedOpen1 = VehicleDistributions.Quad_Trunk,
	TruckBedOpen2 = VehicleDistributions.Quad_Trunk,
	GloveBox = VehicleDistributions.Quad_Trunk,
	SeatFrontRight = VehicleDistributions.Quad_Trunk,
}




	

	VehicleDistributions[1].AME_Patriot = {
		Normal = VehicleDistributions.Patriot,
	}
	VehicleDistributions[1].AME_Eagle = {
		Normal = VehicleDistributions.Eagle,
	}
	VehicleDistributions[1].AM2_Courier = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2_Commodore = {
		Normal = VehicleDistributions.Commodore,
	}
	VehicleDistributions[1].AM2_Balrog_Sidecar = {
		Normal = VehicleDistributions.Sidecar,
	}
	VehicleDistributions[1].AM2_Camel2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2_DirtDemon2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2_Warhorse2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2_Steelhorse2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2_Wendigo = {
		Normal = VehicleDistributions.Quad,
	}
	VehicleDistributions[1].AM2_Yeti = {
		Normal = VehicleDistributions.Quad,
	}


	

	VehicleDistributions[1].AMEPatriot = {
		Normal = VehicleDistributions.Patriot,
	}
	VehicleDistributions[1].AMEEagle = {
		Normal = VehicleDistributions.Eagle,
	}
	VehicleDistributions[1].AM2Courier = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2Commodore = {
		Normal = VehicleDistributions.Commodore,
	}
	VehicleDistributions[1].AM2Balrog_Sidecar = {
		Normal = VehicleDistributions.Sidecar,
	}
	VehicleDistributions[1].AM2Camel2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2DirtDemon2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2Warhorse2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2Steelhorse2 = {
		Normal = VehicleDistributions.Courier,
	}
	VehicleDistributions[1].AM2Wendigo = {
		Normal = VehicleDistributions.Quad,
	}
	VehicleDistributions[1].AM2Yeti = {
		Normal = VehicleDistributions.Quad,
	}
