--***********************************************************
--**                    ROBERT JOHNSON                     **
--***********************************************************

require "TimedActions/ISBaseTimedAction"

AM2_ISTurnVehicle = ISBaseTimedAction:derive("AM2_ISTurnVehicle");

function AM2_ISTurnVehicle:isValid()
	--print("is valid")
	return true
end

function AM2_ISTurnVehicle:update()
	--print("update")
	-- self.character:setIgnoreMovementForDirection(false);
	self.character:faceThisObject(self.vehicle);
	--self.character:setIgnoreMovementForDirection(true);
	local roll = 19
	if ZombRand(roll) == 0 then
		self.vehicle:getEmitter():playSound(self.sound1)
		addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), 20, 10)
	end	
end

function AM2_ISTurnVehicle:start()

		self:setActionAnim("Loot");
		self.character:SetVariable("LootPosition", "Low");
		  -- self:setActionAnim("RemoveBarricade")      
          -- self:setAnimVariable("RemoveBarricade", "CrowbarMid")
   	
	self.vehicle:getEmitter():playSound(self.sound1)
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), 20, 10)

end

function AM2_ISTurnVehicle:stop()
	--print("stop")
	self.character:PlayAnim("Idle");
	self.character:setIgnoreMovementForDirection(false);
    ISBaseTimedAction.stop(self);
end

function AM2_ISTurnVehicle:perform()
	-- --print("perform")
	local direction =  self.vehicle:getDir()
	-- -- print("Drection: " .. tostring(direction))
	-- -- self.vehicle:savedRot().set(self.vehicle:savedRot())
	-- -- print("Var3 2 - " .. tostring(var3))
	-- local var3Abs = math.abs(var3)
	-- -- print("Var3 Abs - " .. tostring(var3Abs))
	-- -- print("Var3 " .. tostring(var3))
    -- self.vehicle:getEmitter():playSound(self.sound2)
    addSound(self.character, self.character:getX(), self.character:getY(), self.character:getZ(), 20, 10)
	Vehicles.AM2_RightBike(self.vehicle, self.debugVersion)
	
	 -- -- self.vehicle:setDir(direction)
	 -- -- self.vehicle:setAngles(var1, var2, 180)
	-- -- local var3 = self.vehicle:getAngleZ()
	-- -- print("Var3 3 - " .. tostring(var3))
	-- if self.debugVersion == true then
		-- print("DEBUG VERSION")
		-- self.vehicle:flipUpright()
		-- -- print("Not debug!")
		-- -- if var3Abs < 90 then 
			-- -- self.vehicle:setAngles(var1, var2, 0)
		-- -- elseif var3Abs > 120 then 
			-- -- self.vehicle:setAngles(var1, var2, 0)
		-- -- else
			-- -- self.vehicle:setAngles(var1, var2, 180)	
		-- -- end
	-- else
	 -- -- self.vehicle:setDir(direction)
		-- local var2 = self.vehicle:getAngleY()
		-- -- self.vehicle:flipUpright()
		-- -- local var1 = self.vehicle:getAngleX()
		-- -- local var3 = self.vehicle:getAngleZ()
			-- -- self.vehicle:setAngles(var1, var2, var3)
			
	 -- self.vehicle:setDir(direction)
			-- self.vehicle:setAngles(0, var2, 0)
	-- end
	-- -- -- local var3 = self.vehicle:getAngleZ()
	-- -- -- print("Var3 4 - " .. tostring(var3))
	ISBaseTimedAction.perform(self);
end

function AM2_ISTurnVehicle:new(player, vehicle, debugVersion)
	--print("new")
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = player;
	o.vehicle = vehicle;
	o.stopOnWalk = true;
	o.stopOnRun = true;
	o.debugVersion = debugVersion
	--o.mass = vehicle:getMass()
	--o.maxTime = 150 - (player:getPerkLevel(Perks.Strength) * 10)
	o.maxTime = (vehicle:getMass()/2) - (player:getPerkLevel(Perks.Strength) * 10)
	if o.maxTime < 50 then o.maxTime = 50 end
	o.spriteFrame = 0
	o.sound1 = "ZombieThumpVehicle"
	o.sound2 = "BreakBarricadeMetal"
	-- local var3 = o.vehicle:getAngleZ()
	-- print("Var3 1 - " .. tostring(var3))
	-- print("debugVersion - " .. tostring(debugVersion))
	return o;
end
