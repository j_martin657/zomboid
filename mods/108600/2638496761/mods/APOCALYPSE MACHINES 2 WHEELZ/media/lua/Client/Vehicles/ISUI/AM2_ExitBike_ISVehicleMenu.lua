--***********************************************************
--**                    THE ALGOL STONED                   **
--***********************************************************
require "Vehicles/ISUI/ISVehicleMenu"

local old_ISVehicleMenu_onExit = ISVehicleMenu.onExit

function ISVehicleMenu.onExit(playerObj, seatFrom)
    local vehicle = playerObj:getVehicle()
	-- print("Exiting " .. tostring(vehicle:getScriptName()))
	
	if vehicle:getScriptName():contains("Commodore") then --or isAdmin()
		old_ISVehicleMenu_onExit(playerObj, seatFrom)
    end
   -- if not vehicle:getScriptName():contains("AM2") and not vehicle:getScriptName():contains("AME") then
   if not vehicle:getScriptName():contains("AM2") then
		old_ISVehicleMenu_onExit(playerObj, seatFrom)
    end
	
	
    vehicle:updateHasExtendOffsetForExit(playerObj)
	if (not playerObj:isBlockMovement()) then
		
		if not vehicle then return end
		if vehicle:getCurrentSpeedKmHour() > 1 or vehicle:getCurrentSpeedKmHour() < -1 then 
			playerObj:Say(getText("IGUI_PlayerText_CanNotExitFromMovingCar"))
            vehicle:updateHasExtendOffsetForExitEnd(playerObj)
			return 
		end
		seatFrom = seatFrom or vehicle:getSeat(playerObj)
		if vehicle:isExitBlocked(playerObj, seatFrom) then
			local seatTo = ISVehicleMenu.getBestSwitchSeatExit(playerObj, vehicle, seatFrom)
			if seatTo then
				ISTimedActionQueue.add(ISSwitchVehicleSeat:new(playerObj, seatTo))
				ISVehicleMenu.onExitAux(playerObj, seatTo)
				return
			else	
				ISVehicleMenu.AM2_onExitAux(playerObj, seatFrom)
			end
		else
			-- print("Supposed to exit vehicle!")
			-- ISVehicleMenu.AM2_onExitAux(playerObj, seatFrom)
			ISVehicleMenu.onExitAux(playerObj, seatFrom)
		end
	end
end

function ISVehicleMenu.AM2_onExitAux(playerObj, seat)
	local vehicle = playerObj:getVehicle()
	local doorPart = vehicle:getPassengerDoor(seat)
	if not vehicle:isExitBlocked(playerObj, seat) then
		if doorPart and doorPart:getDoor() and doorPart:getInventoryItem() then
			local door = doorPart:getDoor()
			if door:isLocked() then
				ISTimedActionQueue.add(ISUnlockVehicleDoor:new(playerObj, doorPart))
			end
			if not door:isOpen() then
				ISTimedActionQueue.add(ISOpenVehicleDoor:new(playerObj, vehicle, seat))
			end
			ISTimedActionQueue.add(ISExitVehicle:new(playerObj))
			ISTimedActionQueue.add(ISCloseVehicleDoor:new(playerObj, vehicle, doorPart))
		else
			ISTimedActionQueue.add(ISExitVehicle:new(playerObj))
		end
	else
		ISTimedActionQueue.add(AM2_ISExitVehicle_Alt:new(playerObj))
	end
end