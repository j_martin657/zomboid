require "Vehicle/ISUI/ISVehicleMenu"


local oldFillMenuOutsideVehicle =ISVehicleMenu.FillMenuOutsideVehicle
function ISVehicleMenu.FillMenuOutsideVehicle(player, context, vehicle, test)
	local playerObj = getSpecificPlayer(player)
    oldFillMenuOutsideVehicle(player, context, vehicle, test)
	--print("TEST 1")	
	if vehicle:getScriptName():contains("Commodore") then --or isAdmin()
		--print("TEST 2")
           -- context:addOption(getText("ContextMenu_TurnMoveBike"), playerObj, AM2_SFDriveFunctions_turnBike, vehicle)
			return
    end
   if vehicle:getScriptName():contains("AM2") or vehicle:getScriptName():contains("AME") then --or isAdmin()
		--print("TEST 2")
		if not vehicle:getScriptName():contains("Commodore") then
			-- local debugVersion = nil
			-- local var3 = math.abs(vehicle:getAngleZ())
			-- print("Var3 1 - " .. tostring(vehicle:getAngleZ()))
			-- if var3 > 75 and var3 <150 then 
				local var3 = math.abs(vehicle:getAngleZ())
				-- print("Var3 1 - " .. tostring(var3))
				if var3 > 45  and var3 < 135 then 
					context:addOption(getText("ContextMenu_TurnMoveBike"), playerObj, AM2_SFDriveFunctions_turnBike, vehicle, false)
				end
			-- end
			-- debugVersion = true
			context:addOption(getText("ContextMenu_TurnMoveBike_Debug"), playerObj, AM2_SFDriveFunctions_turnBike, vehicle, true)
		end
			
    end
	-- local square = playerObj:getSquare()
   -- if vehicle:getScriptName():contains("AME_") and AM2_goodVehicleTile(square) then --or isAdmin() --ADD SQUARE BLOCKED CHECKS!
		-- --print("TEST 2")
            -- context:addOption(getText("ContextMenu_PullDevice"), playerObj, AM2_PullDevice, vehicle)
			
    -- end
end

local old_ISVehicleMenu_showRadialMenuOutside = ISVehicleMenu.showRadialMenuOutside
function ISVehicleMenu.showRadialMenuOutside(playerObj)
	 old_ISVehicleMenu_showRadialMenuOutside(playerObj)
	 local playerIndex = playerObj:getPlayerNum()
	local menu = getPlayerRadialMenu(playerIndex)
	 local vehicle = ISVehicleMenu.getVehicleToInteractWith(playerObj)
	 if not vehicle then return end
	 if not vehicle:getScriptName():contains("AM2") and not vehicle:getScriptName():contains("AME") then --or isAdmin()
		--print("TEST 2")
           -- context:addOption(getText("ContextMenu_TurnMoveBike"), playerObj, AM2_SFDriveFunctions_turnBike, vehicle)
			return
    end
	if vehicle:getScriptName():contains("Commodore") then --or isAdmin()
		--print("TEST 2")
           -- context:addOption(getText("ContextMenu_TurnMoveBike"), playerObj, AM2_SFDriveFunctions_turnBike, vehicle)
			return
    end
	 
			--local menu = getPlayerRadialMenu(player:getPlayerNum())
			if menu then
				-- local var1 = (vehicle:getAngleX())
				-- print("Var1 1 - " .. tostring(var1))
				local var2 = (vehicle:getAngleY())
				-- print("Var2 : " .. tostring(var2))
				-- local var3 = (vehicle:getAngleZ())
				-- print("Var3 1 - " .. tostring(var3))
				-- local var1 = (vehicle:getAngleX())
				-- print("Var1 1 - " .. tostring(var1))
				-- local var2 = (vehicle:getAngleY())
				-- print("Var2 1 - " .. tostring(var2))
				local var3 = math.abs(vehicle:getAngleZ())
				-- print("Var3 1 - " .. tostring(var3))
				if var3 > 45  and var3 < 135 then 
					menu:addSlice(getText("ContextMenu_TurnMoveBike"), getTexture("media/textures/AM2_bike1.png"), AM2_SFDriveFunctions_turnBike, playerObj, vehicle, false)
				end
			end
	-- local square = playerObj:getSquare()
   -- if vehicle:getScriptName():contains("AME_") and AM2_goodVehicleTile(square) then --or isAdmin()
		-- --print("TEST 2")
            -- context:addOption(getText("ContextMenu_PullDevice"),
			-- getTexture("media/textures/AM2_bike1.png"),
			-- AM2_PullDevice,
			-- playerObj,
			-- vehicle)
			
    -- end
end