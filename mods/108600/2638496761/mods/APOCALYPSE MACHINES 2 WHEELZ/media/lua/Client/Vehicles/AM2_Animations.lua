-- this method was copied from the excellent Skateboard mod by Dislaik https://steamcommunity.com/sharedfiles/filedetails/?id=2728300240
-- require "Vehicles/ISUI/ISVehicleMenu"
-- require "Vehicles/TimedActions/ISStartVehicleEngine"

local function PA_Bike_Enter(player)
	local vehicle = player:getVehicle()
	if not vehicle then return end
    local vehicleName = vehicle:getScriptName()
    if not vehicleName:contains("AM2") then return end
    if vehicleName:contains("Commodore") then return end
	sendClientCommand(player, "PA_Bike", "PlayerEnter", {
        vehicleId = vehicle:getId()
    })
    local seat = vehicle:getSeat(player)
    if not seat then return end
	if seat == 1 and vehicleName:contains("Sidecar") then		
		player:SetVariable("VehicleScriptName", "")
		return
	end
    if vehicleName:contains("SkullKing")
	or vehicleName:contains("Camel")
	then
		player:SetVariable("VehicleScriptName", "AM2_Bike_SkullKing")	
    elseif vehicleName:contains("Kaiju") then
		player:SetVariable("VehicleScriptName", "AM2_Bike_Kaiju")	
    elseif vehicleName:contains("DirtDemon")
	or vehicleName:contains("Warhorse")
	or vehicleName:contains("Wendigo")
	or vehicleName:contains("Yeti")	
	then
		player:SetVariable("VehicleScriptName", "AM2_Bike_DirtDemon")		
	else		
		player:SetVariable("VehicleScriptName", "AM2_Bike")
	end
end

-- function PA_Bike_Enter_Server(player)
	-- local vehicle = player:getVehicle()
	-- if not vehicle then return end
    -- local vehicleName = vehicle:getScriptName()
    -- if not vehicleName:contains("AM2") then return end
    -- if vehicleName:contains("Commodore") then return end
	-- -- sendClientCommand(source, "PA_Bike", "PlayerEnter", {
        -- -- vehicleId = vehicle:getId()
    -- -- })
    -- local seat = vehicle:getSeat(player)
    -- if not seat then return end
	-- if seat == 1 and vehicleName:contains("Sidecar") then		
		-- player:SetVariable("VehicleScriptName", "")
		-- return
	-- end
    -- if vehicleName:contains("SkullKing")
	-- or vehicleName:contains("Camel")
	-- then
		-- player:SetVariable("VehicleScriptName", "AM2_Bike_SkullKing")	
    -- elseif vehicleName:contains("Kaiju") then
		-- player:SetVariable("VehicleScriptName", "AM2_Bike_Kaiju")	
    -- elseif vehicleName:contains("DirtDemon")
	-- or vehicleName:contains("Warhorse")
	-- or vehicleName:contains("Wendigo")
	-- or vehicleName:contains("Yeti")	
	-- then
		-- player:SetVariable("VehicleScriptName", "AM2_Bike_DirtDemon")		
	-- else		
		-- player:SetVariable("VehicleScriptName", "AM2_Bike")
	-- end
-- end

local function PA_Bike_Exit(player)
    sendClientCommand(player, "PA_Bike", "PlayerExit", {})
    player:SetVariable("VehicleScriptName", "")
end

-- Events.OnEnterVehicle.Add(PA_Bike_Enter)
Events.OnExitVehicle.Add(PA_Bike_Exit)
-- Events.OnSwitchVehicleSeat.Add(PA_Bike_Enter)

-- local function PA_HopIn(key)
	-- local player = getPlayer()
	-- if not player then return end
	-- local vehicle = player:getVehicle()
	-- if key == getCore():getKey("PA_JumpIn") and not vehicle then
		-- local vehicle = player:getNearVehicle()
		-- if not vehicle then return end	
		-- if vehicle:getScriptName():contains("Commodore") then return end
		-- if not vehicle:getScriptName():contains("AM2") and not vehicle:getScriptName():contains("AME")
		-- then return end
		-- local seat = nil
		-- if not vehicle:getDriver() then seat = 0
		-- elseif not vehicle:isSeatOccupied(1) then seat = 1
		-- elseif not vehicle:isSeatOccupied(2) then seat = 2
		-- else return	end
		-- local distance = math.sqrt(((player:getX() - vehicle:getX())^2) + ((player:getY() - vehicle:getY())^2))
		-- if distance < 1 then
			-- ISTimedActionQueue.add(ISEnterVehicle:new(player, vehicle, seat))
		-- end
	-- end
-- end

-- Events.OnKeyStartPressed.Add(PA_HopIn);
AM2_Crash = {}

-- Events.OnTick.Remove(onTick)
function AM2_onTick(tick)
	-- print("TICK")
	local player = getPlayer(); if not player then return end
	local vehicle = player:getVehicle();
	if not vehicle then
		AM2_Crash.vSpeed = 0
		return
	end
	if vehicle and (vehicle:getScriptName():contains"AM2") then
		local vSpeed = vehicle:getCurrentSpeedKmHour()
		if not AM2_Crash.vSpeed then AM2_Crash.vSpeed = vSpeed return end
		-- print("vSpeed - " .. tostring(math.floor(vSpeed)) .. " - " .. tostring(math.floor(AM2_Crash.vSpeed)) )
		local var3 = math.abs(vehicle:getAngleZ())
		if var3 > 80 and var3 < 100 and ((math.abs(AM2_Crash.vSpeed) - math.abs(vSpeed)) > ( 1 + ZombRand(player:getPerkLevel(Perks.Strength)) ) ) then --+ ZombRand(player:getPerkLevel(Perks.Strength))
				-- print("BAIL")
				-- local var1 = (vehicle:getAngleX())
				-- local var2 = (vehicle:getAngleY())
				-- local var3 = (vehicle:getAngleZ())
				-- print("Vars - " .. tostring(var1) .. " - " .. tostring(var2) .. " - " .. tostring(var3))
				vehicle:exit(player)
				player:setBumpType("stagger")
				player:setBumpFall(true)
				-- player:setBumpFallType("pushedBehind") 
				if ZombRand(0,2) == 0 then
						player:setBumpFallType("pushedFront")
					else
						player:setBumpFallType("pushedBehind")
				end				
				-- vehicle:updateHasExtendOffsetForExitEnd(player)	
			getPlayerVehicleDashboard(player:getPlayerNum()):setVehicle(nil)	

			-- Vehicles.Update.Engine(vehicle, vehicle:getPartById("Engine"), 1)
				AM2_Crash.vSpeed = 0		
		elseif var3 > 75 and var3 < 105 then
			-- -- print("TIP? - " .. tostring(var3).." - " .. tostring(AM2_Crash.vSpeed - vSpeed)) 
			-- -- if AM2_Crash.vSpeed > 10 and (AM2_Crash.vSpeed - vSpeed) > ( 10 + (player:getPerkLevel(Perks.Strength)*2) + ZombRand(10) + (player:getPerkLevel(Perks.Nimble)*2)) then
			if  (math.abs(AM2_Crash.vSpeed) - math.abs(vSpeed)) > (10 + ZombRand(player:getPerkLevel(Perks.Strength) + player:getPerkLevel(Perks.Nimble)) ) then
			-- print("TIP - " .. tostring(AM2_Crash.vSpeed - vSpeed)) 
			-- print("TIP - " .. tostring(vSpeed) .. " - " .. tostring(AM2_Crash.vSpeed)) 
			
				-- local var1 = (vehicle:getAngleX())
				-- local var2 = (vehicle:getAngleY())
				-- local var3 = (vehicle:getAngleZ())
				-- print("Vars - " .. tostring(var1) .. " - " .. tostring(var2) .. " - " .. tostring(var3))
				vehicle:exit(player)
				player:setBumpType("stagger")
				player:setBumpFall(true)
				-- player:setBumpFallType("pushedBehind")
				if ZombRand(0,2) == 0 then
						player:setBumpFallType("pushedFront")
					else
						player:setBumpFallType("pushedBehind") 
					end
				-- vehicle:updateHasExtendOffsetForExitEnd(player)	
			getPlayerVehicleDashboard(player:getPlayerNum()):setVehicle(nil)		
				AM2_Crash.vSpeed = 0
			end
		end
		
		
		if AM2_Crash.vSpeed > 25 and (math.abs(AM2_Crash.vSpeed) - math.abs(vSpeed)) > ( 25 + (player:getPerkLevel(Perks.Strength)*2) + ZombRand(30) + (player:getPerkLevel(Perks.Nimble)*2)) then
			-- print("CRASH - " .. tostring(vSpeed) .. " - " .. tostring(AM2_Crash.vSpeed)) 
			triggerEvent("OnExitVehicle", player)
			vehicle:exit(player)
			player:setBumpType("stagger")
			player:setBumpFall(true)
			player:setBumpFallType("pushedBehind")	
			vehicle:updateHasExtendOffsetForExitEnd(player)	
			getPlayerVehicleDashboard(player:getPlayerNum()):setVehicle(nil)	
			AM2_Crash.vSpeed = 0
		end
			AM2_Crash.vSpeed = vSpeed
	end
end


-- Events.OnTick.Add(onTick)