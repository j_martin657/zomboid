-- this method was copied from the excellent Skateboard mod by Dislaik https://steamcommunity.com/sharedfiles/filedetails/?id=2728300240
-- require "Vehicles/ISUI/ISVehicleMenu"
-- require "Vehicles/TimedActions/ISStartVehicleEngine"

local function PA_Bike_Enter(player)
	local vehicle = player:getVehicle()
	if not vehicle then return end
	
	-- local var3 = vehicle:getAngleZ()
	-- print("var3 - " ..tostring(var3))
    local vehicleName = vehicle:getScriptName()
    if not vehicleName:contains("AM2") then return end
    if vehicleName:contains("Commodore") then return end
	sendClientCommand(player, "PA_Bike", "PlayerEnter", {
        vehicleId = vehicle:getId()
    })
    local seat = vehicle:getSeat(player)
    if not seat then return end
	if seat == 1 and vehicleName:contains("Sidecar") then		
		player:SetVariable("VehicleScriptName", "")
		return
	end
    if vehicleName:contains("SkullKing")
	or vehicleName:contains("Camel")
	-- or vehicleName:contains("Warhorse")
	then
		player:SetVariable("VehicleScriptName", "AM2_Bike_SkullKing")	
    elseif vehicleName:contains("Kaiju") then
		player:SetVariable("VehicleScriptName", "AM2_Bike_Kaiju")	
    elseif vehicleName:contains("DirtDemon")
	or vehicleName:contains("Warhorse")
	or vehicleName:contains("Wendigo")
	or vehicleName:contains("Yeti")	
	then
		player:SetVariable("VehicleScriptName", "AM2_Bike_DirtDemon")		
	else		
		player:SetVariable("VehicleScriptName", "AM2_Bike")
	end
end


local function PA_HopIn(key)
	-- print("Key - " ..tostring(key))
	local player = getPlayer()
	if not player then return end
	local vehicle = player:getVehicle()
	if key == getCore():getKey("PA_JumpIn") and not vehicle then
		local vehicle = player:getNearVehicle()
		if not vehicle then return end	
		if vehicle:getScriptName():contains("Commodore") then return end
		if not vehicle:getScriptName():contains("AM2") and not vehicle:getScriptName():contains("AME")
		then return end
		local seat = nil
		if not vehicle:getDriver() then seat = 0
		elseif not vehicle:isSeatOccupied(1) then seat = 1
		elseif not vehicle:isSeatOccupied(2) then seat = 2
		else return	end
		local distance = math.sqrt(((player:getX() - vehicle:getX())^2) + ((player:getY() - vehicle:getY())^2))
		if distance < 1 then
			ISTimedActionQueue.add(ISEnterVehicle:new(player, vehicle, seat))
		end
	end
end

function PA_Bike_Enter_Server(player)
	PA_Bike_Enter(player)
	-- local vehicle = player:getVehicle()
	-- if not vehicle then return end
    -- local vehicleName = vehicle:getScriptName()
    -- if not vehicleName:contains("AM2") then return end
    -- if vehicleName:contains("Commodore") then return end
	-- -- sendClientCommand(source, "PA_Bike", "PlayerEnter", {
        -- -- vehicleId = vehicle:getId()
    -- -- })
    -- local seat = vehicle:getSeat(player)
    -- if not seat then return end
	-- if seat == 1 and vehicleName:contains("Sidecar") then		
		-- player:SetVariable("VehicleScriptName", "")
		-- return
	-- end
    -- if vehicleName:contains("SkullKing")
	-- or vehicleName:contains("Camel")
	-- then
		-- player:SetVariable("VehicleScriptName", "AM2_Bike_SkullKing")	
    -- elseif vehicleName:contains("Kaiju") then
		-- player:SetVariable("VehicleScriptName", "AM2_Bike_Kaiju")	
    -- elseif vehicleName:contains("DirtDemon")
	-- or vehicleName:contains("Warhorse")
	-- or vehicleName:contains("Wendigo")
	-- or vehicleName:contains("Yeti")	
	-- then
		-- player:SetVariable("VehicleScriptName", "AM2_Bike_DirtDemon")		
	-- else		
		-- player:SetVariable("VehicleScriptName", "AM2_Bike")
	-- end
end



Events.OnEnterVehicle.Add(PA_Bike_Enter)
Events.OnSwitchVehicleSeat.Add(PA_Bike_Enter)
Events.OnKeyStartPressed.Add(PA_HopIn);