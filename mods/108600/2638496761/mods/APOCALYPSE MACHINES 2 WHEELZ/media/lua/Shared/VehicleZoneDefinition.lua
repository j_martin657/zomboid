VehicleZoneDistribution = VehicleZoneDistribution or {};

--[[
Use this file to alter or add vehicle spawning logic.
 the type should be the one you define in the zone in WorldEd (VehicleZoneDistribution.trailerpark will be used for trailerpark zone type)
 if no type is defined in the zone, parkingstall is used instead.
 
 When adding a car, you define it's skin index defined in the vehicle's template (-1 mean a random skin in the skin list)
 spawnChance is used to define the odds of spawning this car or another (the total for a zone should always be 100)
 
 You have a range of variable to configure your spawning logic:
 * chanceToPartDamage : Chance of having a damaged part, this number is added to the inventory item's damaged spawn chance of the part (so an old tire will have more chance to be damaged than a good one in a same zone). Default is 0.
 * baseVehicleQuality : Define the base quality for part, if a part should be spawned as damaged, this will define it's max condition (so a 0.7 mean if a part spawn as damaged, it's max condition will be 70%). Default is 1.0.
 * chanceToSpawnSpecial : Use this to define a random chance of spawning special car (picked randomly in every type with specialCar = true) on a zone. Default is 5.
 * chanceToSpawnBurnt : Use this to define a random chance of spawning burnt car like in junkyard (picked randomly at 80% in normalburnt list & 20% in specialburnt list) on a zone. Default is 0.
 * chanceToSpawnNormal : Use this to define a random chance of spawning a normal car (will be picked in the parkingstall zone). Used so the special parking lots don't have only special cars (so a spiffo parking lot will have lots of normal car, and sometimes a spiffo van). Default is 80(%).
 * spawnRate : Base chance of adding a vehicle in a zone, default is 16(%).
 * chanceOfOverCar : Chance to spawn another car over the spawned one (used in trailerpark). Default is 0.
 * randomAngle : Are cars aligned on a grid or random angle. Default is false.
 * chanceToSpawnKey : Define the chance to spawn a key for this car (either on the ground, directly in the car, in a near zombie or container...) Default is 70(%).
 * specialCar : Define if the car is a special one (police, fire dept...) used to get a list of special car when trying to spawn a special car if chanceToSpawnSpecial is triggered. a special car will also make the corresponding vehicle's key not colored. Can still be used as a normal zone.
 ]]

-- ****************************** --
--          NORMAL VEHICLES       --
-- ****************************** --

-- Parking Stall, common parking stall with random cars, the most used one (shop parking lots, houses etc.)
VehicleZoneDistribution.parkingstall = {};
VehicleZoneDistribution.parkingstall.vehicles = {};
VehicleZoneDistribution.parkingstall.vehicles["Base.CarNormal"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.parkingstall.vehicles["Base.SmallCar"] = {index = -1, spawnChance = 15};
VehicleZoneDistribution.parkingstall.vehicles["Base.SmallCar02"] = {index = -1, spawnChance = 15};
VehicleZoneDistribution.parkingstall.vehicles["Base.CarTaxi"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.CarTaxi2"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.PickUpTruck"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.PickUpVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.CarStationWagon"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.CarStationWagon2"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.VanSeats"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.Van"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.StepVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.parkingstall.vehicles["Base.ModernCar"] = {index = -1, spawnChance = 3};
VehicleZoneDistribution.parkingstall.vehicles["Base.ModernCar02"] = {index = -1, spawnChance = 2};
VehicleZoneDistribution.parkingstall.chanceToPartDamage = 20;
VehicleZoneDistribution.parkingstall.baseVehicleQuality = 0.7;

-- Trailer Parks, have a chance to spawn burnt cars, some on top of each others, it's like a pile of junk cars
VehicleZoneDistribution.trailerpark = {};
VehicleZoneDistribution.trailerpark.vehicles = {};
VehicleZoneDistribution.trailerpark.vehicles["Base.CarNormal"] = {index = -1, spawnChance = 25};
VehicleZoneDistribution.trailerpark.vehicles["Base.SmallCar"] = {index = -1, spawnChance = 30};
VehicleZoneDistribution.trailerpark.vehicles["Base.SmallCar02"] = {index = -1, spawnChance = 30};
VehicleZoneDistribution.trailerpark.vehicles["Base.CarStationWagon"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.trailerpark.vehicles["Base.CarStationWagon2"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.trailerpark.vehicles["Base.StepVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.trailerpark.chanceToSpawnBurnt = 20;
VehicleZoneDistribution.trailerpark.baseVehicleQuality = 0.5;
VehicleZoneDistribution.trailerpark.chanceOfOverCar = 10;
VehicleZoneDistribution.trailerpark.chanceToPartDamage = 20;
VehicleZoneDistribution.trailerpark.randomAngle = true;
VehicleZoneDistribution.trailerpark.chanceToSpawnSpecial = 0;

-- bad vehicles, moslty used in poor area, sometimes around pub etc.
VehicleZoneDistribution.bad = {};
VehicleZoneDistribution.bad.vehicles = {};
VehicleZoneDistribution.bad.vehicles["Base.CarNormal"] = {index = -1, spawnChance = 25};
VehicleZoneDistribution.bad.vehicles["Base.SmallCar"] = {index = -1, spawnChance = 28};
VehicleZoneDistribution.bad.vehicles["Base.SmallCar02"] = {index = -1, spawnChance = 28};
VehicleZoneDistribution.bad.vehicles["Base.CarStationWagon"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.bad.vehicles["Base.CarStationWagon2"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.bad.vehicles["Base.StepVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.bad.vehicles["Base.Van"] = {index = -1, spawnChance = 4};
VehicleZoneDistribution.bad.baseVehicleQuality = 0.5;
VehicleZoneDistribution.bad.chanceToSpawnSpecial = 1;

-- medium vehicles, used in some of the good looking area, or in suburbs
VehicleZoneDistribution.medium = {};
VehicleZoneDistribution.medium.vehicles = {};
VehicleZoneDistribution.medium.vehicles["Base.CarNormal"] = {index = -1, spawnChance = 30};
VehicleZoneDistribution.medium.vehicles["Base.CarStationWagon"] = {index = -1, spawnChance = 8};
VehicleZoneDistribution.medium.vehicles["Base.CarStationWagon2"] = {index = -1, spawnChance = 8};
VehicleZoneDistribution.medium.vehicles["Base.PickUpTruck"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.PickUpVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.VanSeats"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.Van"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.StepVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.VanSeats"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.SUV"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.OffRoad"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.ModernCar"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.ModernCar02"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.medium.vehicles["Base.CarLuxury"] = {index = -1, spawnChance = 4};
VehicleZoneDistribution.medium.baseVehicleQuality = 0.8;

-- good vehicles, used in good looking area, they're meant to spawn only good cars, so they're on every good looking house.
VehicleZoneDistribution.good = {};
VehicleZoneDistribution.good.vehicles = {};
VehicleZoneDistribution.good.vehicles["Base.ModernCar"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.good.vehicles["Base.ModernCar02"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.good.vehicles["Base.SUV"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.good.vehicles["Base.OffRoad"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.good.vehicles["Base.CarLuxury"] = {index = -1, spawnChance = 10};
VehicleZoneDistribution.good.vehicles["Base.SportsCar"] = {index = -1, spawnChance = 10};
VehicleZoneDistribution.good.baseVehicleQuality = 1.1;
VehicleZoneDistribution.good.spawnRate = 8; -- less chance to spawn good vehicles (as if they were stolen, or rich people took them already)
VehicleZoneDistribution.trailerpark.chanceToSpawnSpecial = 0;

-- sports vehicles, sometimes on good looking area.
VehicleZoneDistribution.sport = {};
VehicleZoneDistribution.sport.vehicles = {};
VehicleZoneDistribution.sport.vehicles["Base.CarLuxury"] = {index = -1, spawnChance = 50};
VehicleZoneDistribution.sport.vehicles["Base.SportsCar"] = {index = -1, spawnChance = 50};
VehicleZoneDistribution.good.baseVehicleQuality = 1.2;
VehicleZoneDistribution.trailerpark.chanceToSpawnSpecial = 0;

-- junkyard, spawn damaged & burnt vehicles, less chance of finding keys but more cars.
-- also used for the random car crash.
VehicleZoneDistribution.junkyard = {};
VehicleZoneDistribution.junkyard.vehicles = {};
VehicleZoneDistribution.junkyard.vehicles["Base.CarNormal"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.junkyard.vehicles["Base.SmallCar"] = {index = -1, spawnChance = 15};
VehicleZoneDistribution.junkyard.vehicles["Base.SmallCar02"] = {index = -1, spawnChance = 15};
VehicleZoneDistribution.junkyard.vehicles["Base.CarTaxi"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.CarTaxi2"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.PickUpTruck"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.PickUpVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.CarStationWagon"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.CarStationWagon2"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.VanSeats"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.Van"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.StepVan"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.junkyard.vehicles["Base.ModernCar"] = {index = -1, spawnChance = 3};
VehicleZoneDistribution.junkyard.vehicles["Base.ModernCar02"] = {index = -1, spawnChance = 2};
VehicleZoneDistribution.junkyard.chanceToSpawnBurnt = 40;
VehicleZoneDistribution.junkyard.spawnRate = 25;
VehicleZoneDistribution.junkyard.chanceToPartDamage = 30;
VehicleZoneDistribution.junkyard.baseVehicleQuality = 0.2;
VehicleZoneDistribution.junkyard.chanceToSpawnKey = 20;

-- traffic jam, mostly burnt car & damaged ones.
-- Used either for hard coded big traffic jam or smaller random ones.
local trafficjamVehicles = {};
trafficjamVehicles["Base.CarNormal"] = {index = -1, spawnChance = 20};
trafficjamVehicles["Base.SmallCar"] = {index = -1, spawnChance = 15};
trafficjamVehicles["Base.SmallCar02"] = {index = -1, spawnChance = 15};
trafficjamVehicles["Base.CarTaxi"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.CarTaxi2"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.PickUpTruck"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.PickUpVan"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.CarStationWagon"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.CarStationWagon2"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.VanSeats"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.Van"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.StepVan"] = {index = -1, spawnChance = 5};
trafficjamVehicles["Base.ModernCar"] = {index = -1, spawnChance = 3};
trafficjamVehicles["Base.ModernCar02"] = {index = -1, spawnChance = 2};

VehicleZoneDistribution.trafficjamw = {};
VehicleZoneDistribution.trafficjamw.vehicles = trafficjamVehicles;
VehicleZoneDistribution.trafficjamw.chanceToSpawnBurnt = 80;
VehicleZoneDistribution.trafficjamw.baseVehicleQuality = 0.3;
VehicleZoneDistribution.trafficjamw.chanceToPartDamage = 80;
VehicleZoneDistribution.trafficjamw.chanceToSpawnKey = 20;

VehicleZoneDistribution.trafficjame = {};
VehicleZoneDistribution.trafficjame.vehicles = trafficjamVehicles;
VehicleZoneDistribution.trafficjame.chanceToSpawnBurnt = 80;
VehicleZoneDistribution.trafficjame.baseVehicleQuality = 0.3;
VehicleZoneDistribution.trafficjame.chanceToPartDamage = 80;
VehicleZoneDistribution.trafficjame.chanceToSpawnKey = 20;

VehicleZoneDistribution.trafficjamn = {};
VehicleZoneDistribution.trafficjamn.vehicles = trafficjamVehicles;
VehicleZoneDistribution.trafficjamn.chanceToSpawnBurnt = 80;
VehicleZoneDistribution.trafficjamn.baseVehicleQuality = 0.3;
VehicleZoneDistribution.trafficjamn.chanceToPartDamage = 80;
VehicleZoneDistribution.trafficjamn.chanceToSpawnKey = 20;

VehicleZoneDistribution.trafficjams = {};
VehicleZoneDistribution.trafficjams.vehicles = trafficjamVehicles;
VehicleZoneDistribution.trafficjams.chanceToSpawnBurnt = 80;
VehicleZoneDistribution.trafficjams.baseVehicleQuality = 0.3;
VehicleZoneDistribution.trafficjams.chanceToPartDamage = 80;
VehicleZoneDistribution.trafficjams.chanceToSpawnKey = 20;

-- ****************************** --
--          SPECIAL VEHICLES      --
-- ****************************** --

-- police
VehicleZoneDistribution.police = {};
VehicleZoneDistribution.police.vehicles = {};
VehicleZoneDistribution.police.vehicles["Base.PickUpVanLightsPolice"] = {index = 0, spawnChance = 40};
VehicleZoneDistribution.police.vehicles["Base.CarLightsPolice"] = {index = 0, spawnChance = 60};
VehicleZoneDistribution.police.chanceToSpawnNormal = 70;
VehicleZoneDistribution.police.specialCar = true;

-- fire dept
VehicleZoneDistribution.fire = {};
VehicleZoneDistribution.fire.vehicles = {};
VehicleZoneDistribution.fire.vehicles["Base.PickUpVanLightsFire"] = {index = -1, spawnChance = 50};
VehicleZoneDistribution.fire.vehicles["Base.PickUpTruckLightsFire"] = {index = -1, spawnChance = 50};
VehicleZoneDistribution.fire.specialCar = true;

-- ranger
VehicleZoneDistribution.ranger = {};
VehicleZoneDistribution.ranger.vehicles = {};
VehicleZoneDistribution.ranger.vehicles["Base.CarLights"] = {index = 0, spawnChance = 50};
VehicleZoneDistribution.ranger.vehicles["Base.PickUpVanLights"] = {index = 0, spawnChance = 25};
VehicleZoneDistribution.ranger.vehicles["Base.PickUpTruckLights"] = {index = 0, spawnChance = 25};
VehicleZoneDistribution.ranger.specialCar = true;

-- mccoy
VehicleZoneDistribution.mccoy = {};
VehicleZoneDistribution.mccoy.vehicles = {};
VehicleZoneDistribution.mccoy.vehicles["Base.PickUpVanMccoy"] = {index = 2, spawnChance = 50};
VehicleZoneDistribution.mccoy.vehicles["Base.PickUpTruckMccoy"] = {index = 2, spawnChance = 50};
VehicleZoneDistribution.mccoy.vehicles["Base.VanSpecial"] = {index = 1, spawnChance = 50};
VehicleZoneDistribution.mccoy.specialCar = true;

-- postal (mail)
VehicleZoneDistribution.postal = {};
VehicleZoneDistribution.postal.vehicles = {};
VehicleZoneDistribution.postal.vehicles["Base.StepVanMail"] = {index = -1, spawnChance = 50};
VehicleZoneDistribution.postal.vehicles["Base.VanSpecial"] = {index = 2, spawnChance = 50};
VehicleZoneDistribution.postal.specialCar = true;

-- spiffo
VehicleZoneDistribution.spiffo = {};
VehicleZoneDistribution.spiffo.vehicles = {};
VehicleZoneDistribution.spiffo.vehicles["Base.VanSpiffo"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.spiffo.specialCar = true;

-- ambulance
VehicleZoneDistribution.ambulance = {};
VehicleZoneDistribution.ambulance.vehicles = {};
VehicleZoneDistribution.ambulance.vehicles["Base.VanAmbulance"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.ambulance.specialCar = true;

-- radio
VehicleZoneDistribution.radio = {};
VehicleZoneDistribution.radio.vehicles = {};
VehicleZoneDistribution.radio.vehicles["Base.VanRadio"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.radio.specialCar = true;

-- fossoil
VehicleZoneDistribution.fossoil = {};
VehicleZoneDistribution.fossoil.vehicles = {};
VehicleZoneDistribution.fossoil.vehicles["Base.PickUpVanLights"] = {index = 1, spawnChance = 33};
VehicleZoneDistribution.fossoil.vehicles["Base.PickUpTruckLights"] = {index = 1, spawnChance = 33};
VehicleZoneDistribution.fossoil.vehicles["Base.VanSpecial"] = {index = 0, spawnChance = 34};
VehicleZoneDistribution.fossoil.specialCar = true;

-- scarlet dist
VehicleZoneDistribution.scarlet = {};
VehicleZoneDistribution.scarlet.vehicles = {};
VehicleZoneDistribution.scarlet.vehicles["Base.StepVan_Scarlet"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.scarlet.specialCar = true;
VehicleZoneDistribution.scarlet.chanceToSpawnNormal = 40;

-- mass genfac co.
VehicleZoneDistribution.massgenfac = {};
VehicleZoneDistribution.massgenfac.vehicles = {};
VehicleZoneDistribution.massgenfac.vehicles["Base.Van_MassGenFac"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.massgenfac.specialCar = true;
VehicleZoneDistribution.massgenfac.chanceToSpawnNormal = 60;

-- transit
VehicleZoneDistribution.transit = {};
VehicleZoneDistribution.transit.vehicles = {};
VehicleZoneDistribution.transit.vehicles["Base.Van_Transit"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.transit.specialCar = true;
VehicleZoneDistribution.transit.chanceToSpawnNormal = 60;

-- 3Network
VehicleZoneDistribution.network3 = {};
VehicleZoneDistribution.network3.vehicles = {};
VehicleZoneDistribution.network3.vehicles["Base.VanRadio_3N"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.network3.specialCar = true;
VehicleZoneDistribution.network3.chanceToSpawnNormal = 60;

-- KY Heralds
VehicleZoneDistribution.kyheralds = {};
VehicleZoneDistribution.kyheralds.vehicles = {};
VehicleZoneDistribution.kyheralds.vehicles["Base.StepVan_Heralds"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.kyheralds.specialCar = true;

-- LectroMax
VehicleZoneDistribution.lectromax = {};
VehicleZoneDistribution.lectromax.vehicles = {};
VehicleZoneDistribution.lectromax.vehicles["Base.Van_LectroMax"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.lectromax.specialCar = true;

-- Knox Distillery
VehicleZoneDistribution.knoxdisti = {};
VehicleZoneDistribution.knoxdisti.vehicles = {};
VehicleZoneDistribution.knoxdisti.vehicles["Base.Van_KnoxDisti"] = {index = -1, spawnChance = 100};
VehicleZoneDistribution.knoxdisti.specialCar = true;


-- ****************************** --
--          BURNT VEHICLES        --
-- ****************************** --

-- when spawning burnt vehicles for any zones, 20% will be pick in special burnt vehicles and 80% in the normal burnt vehicles list.

-- normal burnt cars.
VehicleZoneDistribution.normalburnt = {};
VehicleZoneDistribution.normalburnt.vehicles = {};
VehicleZoneDistribution.normalburnt.vehicles["Base.CarNormalBurnt"] = {index = -1, spawnChance = 25};
VehicleZoneDistribution.normalburnt.vehicles["Base.SmallCarBurnt"] = {index = -1, spawnChance = 10};
VehicleZoneDistribution.normalburnt.vehicles["Base.SmallCar02Burnt"] = {index = -1, spawnChance = 10};
VehicleZoneDistribution.normalburnt.vehicles["Base.OffRoadBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.PickupBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.PickUpVanBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.SportsCarBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.VanSeatsBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.VanBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.ModernCarBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.ModernCar02Burnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.SUVBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.TaxiBurnt"] = {index = -1, spawnChance = 5};
VehicleZoneDistribution.normalburnt.vehicles["Base.LuxuryCarBurnt"] = {index = -1, spawnChance = 5};

-- special burnt cars.
VehicleZoneDistribution.specialburnt = {};
VehicleZoneDistribution.specialburnt.vehicles = {};
VehicleZoneDistribution.specialburnt.vehicles["Base.NormalCarBurntPolice"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.specialburnt.vehicles["Base.AmbulanceBurnt"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.specialburnt.vehicles["Base.VanRadioBurnt"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.specialburnt.vehicles["Base.PickupSpecialBurnt"] = {index = -1, spawnChance = 20};
VehicleZoneDistribution.specialburnt.vehicles["Base.PickUpVanLightsBurnt"] = {index = -1, spawnChance = 20};
-- require "VehicleZoneDefinition"
--if VehicleZoneDistribution then 
-- VehicleZoneDistribution = VehicleZoneDistribution or {}
-- Parking Stall, common parking stall with random cars, the most used one (shop parking lots, houses etc.)
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.2}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.05}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 0.75}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 0.75}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 1.0}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 0.75}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.125}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.06}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.06}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 0.375}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 0.375}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 0.75}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.125}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.125}
VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 0.75}


-- Trailer Parks, have a chance to spawn burnt cars, some on top of each others, it's like a pile of junk cars

VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 7.5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 10}





-- bad vehicles, moslty used in poor area, sometimes around pub etc.
VehicleZoneDistribution.bad.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.4}
VehicleZoneDistribution.bad.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.1}
VehicleZoneDistribution.bad.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.125}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.125}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 1.25}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 1.25}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.bad.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.25}
-- VehicleZoneDistribution.bad.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.25}


-- medium vehicles, used in some of the good looking area, or in suburbs

VehicleZoneDistribution.medium.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.medium.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 2.5}


-- -- good vehicles, used in good looking area, they're meant to spawn only good cars, so they're on every good looking house.

-- VehicleZoneDistribution.good.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 5}
-- -- VehicleZoneDistribution.good.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 2.5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 2.5}

-- sports vehicles, sometimes on good looking area.
--VehicleZoneDistribution.sport.vehicles["Base.AM2_77transam"] = {index = -1, spawnChance = 1};
VehicleZoneDistribution.sport.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 1.25}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 1.25}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 10}

VehicleZoneDistribution.sport.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.sport.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}


VehicleZoneDistribution.junkyard.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.75}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 2}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 2}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.5}
-- VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 1}

VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 1}


-- traffic jam, mostly burnt car & damaged ones.
-- Used either for hard coded big traffic jam or smaller random ones.
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.75}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.125}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.05}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.25}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 1}
-- VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 0.25}
-- VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 0.5}
-- VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.125}

-- VehicleZoneDistribution.trafficjame.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 1}
-- VehicleZoneDistribution.trafficjame.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.25}
-- VehicleZoneDistribution.trafficjame.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.125}
-- VehicleZoneDistribution.trafficjame.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 1}
-- -- VehicleZoneDistribution.trafficjame.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.125}

-- VehicleZoneDistribution.trafficjamn.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 1}
-- VehicleZoneDistribution.trafficjamn.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.25}
-- VehicleZoneDistribution.trafficjamn.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.125}
-- VehicleZoneDistribution.trafficjamn.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 1}
-- -- VehicleZoneDistribution.trafficjamn.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.125}

-- VehicleZoneDistribution.trafficjams.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 1}
-- VehicleZoneDistribution.trafficjams.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.25}
-- VehicleZoneDistribution.trafficjams.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.125}
-- VehicleZoneDistribution.trafficjams.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 1}
-- -- VehicleZoneDistribution.trafficjams.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.125}

VehicleZoneDistribution.ranger.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.ranger.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}

VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 25}
VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 12.5}
VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 12.5}
VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}

VehicleZoneDistribution.police.vehicles["Base.AM2_Warhorse2"] = {index = 0, spawnChance = 5}
VehicleZoneDistribution.police.vehicles["Base.AM2_Warhorse3"] = {index = 0, spawnChance = 5}



VehicleZoneDistribution.farm = VehicleZoneDistribution.farm or {}
VehicleZoneDistribution.farm.vehicles = VehicleZoneDistribution.farm.vehicles or {}
VehicleZoneDistribution.farm.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 2}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 3}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 2}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Vesper_3"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.farm.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 1}
VehicleZoneDistribution.farm.baseVehicleQuality = 0.8;
VehicleZoneDistribution.farm.chanceToPartDamage = 20;
VehicleZoneDistribution.farm.chanceToSpawnSpecial = 0;
VehicleZoneDistribution.farm.spawnRate = 25;


VehicleZoneDistribution.military = VehicleZoneDistribution.military or {}
VehicleZoneDistribution.military.vehicles = VehicleZoneDistribution.military.vehicles or {}
VehicleZoneDistribution.military.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.military.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.military.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.military.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.military.baseVehicleQuality = 1;
VehicleZoneDistribution.military.chanceToSpawnSpecial = 0;
VehicleZoneDistribution.military.spawnRate = 25;

--end
VehicleZoneDistribution.pa_golfcart = VehicleZoneDistribution.pa_golfcart or {}
VehicleZoneDistribution.pa_golfcart.vehicles = VehicleZoneDistribution.pa_golfcart.vehicles or {}
VehicleZoneDistribution.pa_golfcart.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 50}
VehicleZoneDistribution.pa_golfcart.baseVehicleQuality = 1.1;
-- VehicleZoneDistribution.golfcart.chanceToSpawnSpecial = 0;
VehicleZoneDistribution.pa_golfcart.spawnRate = 25;
-- VehicleZoneDistribution.golfcart.spawnRate = 20000;


VehicleZoneDistribution.pa_utility_carts = VehicleZoneDistribution.pa_utility_carts or {}
VehicleZoneDistribution.pa_utility_carts.vehicles = VehicleZoneDistribution.pa_utility_carts.vehicles or {}
VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 50}
VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 25}
VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 25}
VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 10}
-- VehicleZoneDistribution.carts.chanceToSpawnSpecial = 0;
VehicleZoneDistribution.pa_utility_carts.spawnRate = 25;
-- VehicleZoneDistribution.carts.spawnRate = 20000;


VehicleZoneDistribution.pa_wheelchairs = VehicleZoneDistribution.pa_wheelchairs or {}
VehicleZoneDistribution.pa_wheelchairs.vehicles = VehicleZoneDistribution.pa_wheelchairs.vehicles or {}
VehicleZoneDistribution.pa_wheelchairs.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 50}
VehicleZoneDistribution.pa_wheelchairs.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 5}
VehicleZoneDistribution.pa_wheelchairs.spawnRate = 25

-- VehicleZoneDistribution.golfcart = VehicleZoneDistribution.sport

function PA_golfcart_Zones()
	local dirs = getLotDirectories()
    for i=dirs:size(),1,-1 do
        local map = dirs:get(i-1)
		if map == "Muldraugh, KY" then
			getWorld():registerVehiclesZone( "pa_golfcart", "ParkingStall", 5973, 6512, 0, 5, 12, { Direction = "E" }) -- country club sheds
			getWorld():registerVehiclesZone( "pa_golfcart", "ParkingStall", 5980, 6513, 0, 5, 12, { Direction = "E" }) 
			getWorld():registerVehiclesZone( "pa_golfcart", "ParkingStall", 6164, 6366, 0, 11, 5, { Direction = "S" }) -- country club canteen
			getWorld():registerVehiclesZone( "pa_golfcart", "ParkingStall", 5757, 6391, 0, 10, 3, { Direction = "S" }) -- country club gymns	
			getWorld():registerVehiclesZone( "pa_golfcart", "ParkingStall", 12999, 2262, 0, 10, 3, { Direction = "S" }) -- lv golf club
			getWorld():registerVehiclesZone( "pa_golfcart", "ParkingStall", 12999, 2265, 0, 10, 3, { Direction = "S" })			
			
			getWorld():registerVehiclesZone( "pa_utility_carts", "ParkingStall", 13400, 2365, 0, 3, 6, { Direction = "W" }) -- lv part center
			getWorld():registerVehiclesZone( "pa_utility_carts", "ParkingStall", 13054, 2803, 0, 3, 8, { Direction = "W" }) -- lv petting zoo?
			
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12364, 3667, 0, 3, 6, { Direction = "W" }) -- lv hosptil W
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12364, 3675, 0, 3, 7, { Direction = "W" }) -- lv hosptil W
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12362, 3657, 0, 3, 10, { Direction = "W" }) -- lv hosptil W
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12362, 3682, 0, 3, 10, { Direction = "W" }) -- lv hosptil W
			
			
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12365, 3693, 0, 8, 3, { Direction = "S" }) -- lv hosptil W
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12370, 3696, 0, 3, 9, { Direction = "W" }) -- lv hosptil W
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12370, 3706, 0, 3, 16, { Direction = "W" }) -- lv hosptil W
			
			
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12458, 3671, 0, 3, 17, { Direction = "E" }) -- lv hosptil E
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3692, 0, 3, 3, { Direction = "E" }) -- lv hosptil E
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3697, 0, 3, 3, { Direction = "E" }) -- lv hosptil E
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3703, 0, 3, 3, { Direction = "E" }) -- lv hosptil E
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3708, 0, 3, 3, { Direction = "E" }) -- lv hosptil E
			
			
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12366, 3640, 0, 23, 3, { Direction = "S" }) -- lv hosptil s
			getWorld():registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12390, 3640, 0, 15, 3, { Direction = "S" }) -- lv hosptil s
		end
	end
end

Events.OnLoadMapZones.Add(PA_golfcart_Zones)
-- require "VehicleZoneDefinition"
--if VehicleZoneDistribution then 
-- VehicleZoneDistribution = VehicleZoneDistribution or {}
-- Parking Stall, common parking stall with random cars, the most used one (shop parking lots, houses etc.)
VehicleZoneDistribution.parkingstall.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 1.0}
VehicleZoneDistribution.parkingstall.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 0.5}


-- Trailer Parks, have a chance to spawn burnt cars, some on top of each others, it's like a pile of junk cars
VehicleZoneDistribution.trailerpark.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 10}
VehicleZoneDistribution.trailerpark.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 10}

-- bad vehicles, moslty used in poor area, sometimes around pub etc.
VehicleZoneDistribution.bad.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 2.5}
VehicleZoneDistribution.bad.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 1}

-- medium vehicles, used in some of the good looking area, or in suburbs
--VehicleZoneDistribution.medium.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 5}


-- -- good vehicles, used in good looking area, they're meant to spawn only good cars, so they're on every good looking house.
-- VehicleZoneDistribution.good.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 10}


VehicleZoneDistribution.junkyard.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 0.25}


VehicleZoneDistribution.ambulance.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 20}