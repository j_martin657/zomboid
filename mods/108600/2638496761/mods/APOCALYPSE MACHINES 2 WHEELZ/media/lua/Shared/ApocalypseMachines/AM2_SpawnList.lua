
if VehicleZoneDistribution then 

	VehicleZoneDistribution.farm = VehicleZoneDistribution.farm or {}
	VehicleZoneDistribution.farm.vehicles = VehicleZoneDistribution.farm.vehicles or {}
	VehicleZoneDistribution.farm.baseVehicleQuality = 0.8
	VehicleZoneDistribution.farm.chanceToPartDamage = 20
	VehicleZoneDistribution.farm.chanceToSpawnSpecial = 0
	VehicleZoneDistribution.farm.spawnRate = 25
	VehicleZoneDistribution.military = VehicleZoneDistribution.military or {}
	VehicleZoneDistribution.military.vehicles = VehicleZoneDistribution.military.vehicles or {}
	VehicleZoneDistribution.military.vehicles["Base.AMI_BulletAnt"] = {index = -1, spawnChance = 40}
	VehicleZoneDistribution.military.baseVehicleQuality = 1;
	VehicleZoneDistribution.military.chanceToSpawnSpecial = 0;
	VehicleZoneDistribution.military.spawnRate = 25;
		
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.2}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.05}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 0.75}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 0.75}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 1.0}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 0.75}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.125}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.06}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.06}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 0.375}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 0.375}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 0.75}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.125}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.125}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 0.75}

	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 7.5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 10}

	VehicleZoneDistribution.bad.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.4}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.1}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.125}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.125}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 1.25}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 1.25}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.bad.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.25}	

	VehicleZoneDistribution.medium.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.medium.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 2.5}

-- VehicleZoneDistribution.good.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 5}
-- -- VehicleZoneDistribution.good.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 2.5}
-- VehicleZoneDistribution.good.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 2.5}

	VehicleZoneDistribution.sport.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 1.25}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 1.25}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.sport.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}

	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.75}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.junkyard.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 1}

	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 0.75}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 0.125}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 0.05}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.25}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.trafficjamw.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 1}

	VehicleZoneDistribution.ranger.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.ranger.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}

	VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 25}
	VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 12.5}
	VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 12.5}
	VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.mccoy.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 10}

	VehicleZoneDistribution.police.vehicles["Base.AM2_Warhorse2"] = {index = 0, spawnChance = 5}
	VehicleZoneDistribution.police.vehicles["Base.AM2_Warhorse3"] = {index = 0, spawnChance = 5}

	VehicleZoneDistribution.farm.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Vesper"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Vesper2"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 0.5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 3}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Vesper_3"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.farm.vehicles["Base.AM2_Kaiju"] = {index = -1, spawnChance = 1}

	VehicleZoneDistribution.military.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.military.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.military.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.military.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 5}
	
	VehicleZoneDistribution.parkingstall.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 1.0}
	VehicleZoneDistribution.parkingstall.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 0.5}

	VehicleZoneDistribution.trailerpark.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.trailerpark.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 10}

	VehicleZoneDistribution.bad.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 2.5}
	VehicleZoneDistribution.bad.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 1}

	-- medium vehicles, used in some of the good looking area, or in suburbs
	--VehicleZoneDistribution.medium.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 5}
	-- VehicleZoneDistribution.good.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 10}

	VehicleZoneDistribution.ambulance.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 20}
	
	
	VehicleZoneDistribution.pa_golfcart = VehicleZoneDistribution.pa_golfcart or {}
	VehicleZoneDistribution.pa_golfcart.vehicles = VehicleZoneDistribution.pa_golfcart.vehicles or {}
	VehicleZoneDistribution.pa_golfcart.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 50}
	VehicleZoneDistribution.pa_golfcart.baseVehicleQuality = 1.1;
	VehicleZoneDistribution.pa_golfcart.spawnRate = 25;


	VehicleZoneDistribution.pa_utility_carts = VehicleZoneDistribution.pa_utility_carts or {}
	VehicleZoneDistribution.pa_utility_carts.vehicles = VehicleZoneDistribution.pa_utility_carts.vehicles or {}
	VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 50}
	VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 25}
	VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 25}
	VehicleZoneDistribution.pa_utility_carts.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.pa_utility_carts.spawnRate = 25;

	VehicleZoneDistribution.pa_ranger_carts = VehicleZoneDistribution.pa_ranger_carts or {}
	VehicleZoneDistribution.pa_ranger_carts.vehicles = VehicleZoneDistribution.pa_ranger_carts.vehicles or {}
	-- VehicleZoneDistribution.pa_ranger_carts.vehicles["Base.AM2_Commodore"] = {index = -1, spawnChance = 50}
	VehicleZoneDistribution.pa_ranger_carts.vehicles["Base.AM2_Wendigo"] = {index = -1, spawnChance = 25}
	VehicleZoneDistribution.pa_ranger_carts.vehicles["Base.AM2_Yeti"] = {index = -1, spawnChance = 25}
	VehicleZoneDistribution.pa_ranger_carts.vehicles["Base.AM2_Vesper3"] = {index = -1, spawnChance = 10}
	VehicleZoneDistribution.pa_ranger_carts.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.pa_ranger_carts.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.pa_ranger_carts.spawnRate = 25;
	-- VehicleZoneDistribution.pa_ranger_carts.spawnRate = 750000;

	VehicleZoneDistribution.pa_wheelchairs = VehicleZoneDistribution.pa_wheelchairs or {}
	VehicleZoneDistribution.pa_wheelchairs.vehicles = VehicleZoneDistribution.pa_wheelchairs.vehicles or {}
	VehicleZoneDistribution.pa_wheelchairs.vehicles["Base.AME_Patriot"] = {index = -1, spawnChance = 50}
	VehicleZoneDistribution.pa_wheelchairs.vehicles["Base.AME_Eagle"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.pa_wheelchairs.spawnRate = 25
	-- VehicleZoneDistribution.pa_wheelchairs.spawnRate = 25000
	
	
	VehicleZoneDistribution.pa_bikers = VehicleZoneDistribution.pa_bikers or {}
	VehicleZoneDistribution.pa_bikers.vehicles = VehicleZoneDistribution.pa_bikers.vehicles or {}
	
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_DirtDemon"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_DirtDemon2"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_SkullKing"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Balrog"] = {index = -1, spawnChance = 5}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Balrog_Sidecar"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Fireball"] = {index = -1, spawnChance = 12}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Warhorse"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Warhorse2"] = {index = -1, spawnChance = 1}
	-- VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Warhorse3"] = {index = -1, spawnChance = 1}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Camel"] = {index = -1, spawnChance = 13}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Camel2"] = {index = -1, spawnChance = 2}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Courier"] = {index = -1, spawnChance = 6}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Steelhorse"] = {index = -1, spawnChance = 3}
	VehicleZoneDistribution.pa_bikers.vehicles["Base.AM2_Steelhorse2"] = {index = -1, spawnChance = 2}	
	-- VehicleZoneDistribution.pa_bikers.spawnRate = 25000
	VehicleZoneDistribution.pa_bikers.spawnRate = 25
	-- VehicleZoneDistribution.pa_bikers.spawnRate = 50

	function PA_golfcart_Zones()
		local dirs = getLotDirectories()
		for i=dirs:size(),1,-1 do
			local map = dirs:get(i-1)
			if map == "Muldraugh, KY" then
				local spawnWorld = getWorld()
				spawnWorld:registerVehiclesZone( "pa_golfcart", "ParkingStall", 5973, 6512, 0, 5, 12, { Direction = "E" }) -- country club sheds
				spawnWorld:registerVehiclesZone( "pa_golfcart", "ParkingStall", 5980, 6513, 0, 5, 12, { Direction = "E" }) 
				spawnWorld:registerVehiclesZone( "pa_golfcart", "ParkingStall", 6164, 6366, 0, 11, 5, { Direction = "S" }) -- country club canteen
				spawnWorld:registerVehiclesZone( "pa_golfcart", "ParkingStall", 5757, 6391, 0, 10, 3, { Direction = "S" }) -- country club gymns	
				spawnWorld:registerVehiclesZone( "pa_golfcart", "ParkingStall", 12999, 2262, 0, 10, 3, { Direction = "S" }) -- lv golf club
				spawnWorld:registerVehiclesZone( "pa_golfcart", "ParkingStall", 12999, 2265, 0, 10, 3, { Direction = "S" })			
				
				spawnWorld:registerVehiclesZone( "pa_utility_carts", "ParkingStall", 13400, 2365, 0, 3, 6, { Direction = "W" }) -- lv part center
				spawnWorld:registerVehiclesZone( "pa_utility_carts", "ParkingStall", 13054, 2803, 0, 3, 8, { Direction = "W" }) -- lv petting zoo?
				
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12364, 3667, 0, 3, 6, { Direction = "W" }) -- lv hospital W
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12364, 3675, 0, 3, 7, { Direction = "W" }) -- lv hospital W
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12362, 3657, 0, 3, 10, { Direction = "W" }) -- lv hospital W
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12362, 3682, 0, 3, 10, { Direction = "W" }) -- lv hospital W
				
				
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12365, 3693, 0, 8, 3, { Direction = "S" }) -- lv hospital W
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12370, 3696, 0, 3, 9, { Direction = "W" }) -- lv hospital W
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12370, 3706, 0, 3, 16, { Direction = "W" }) -- lv hospital W
				
				
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12458, 3671, 0, 3, 17, { Direction = "E" }) -- lv hospital E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3692, 0, 3, 3, { Direction = "E" }) -- lv hospital E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3697, 0, 3, 3, { Direction = "E" }) -- lv hospital E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3703, 0, 3, 3, { Direction = "E" }) -- lv hospital E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12456, 3708, 0, 3, 3, { Direction = "E" }) -- lv hospital E
				
				
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12366, 3640, 0, 23, 3, { Direction = "S" }) -- lv hospital s
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12390, 3640, 0, 15, 3, { Direction = "S" }) -- lv hospital s
				
				
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12923, 2090, 0, 7, 3, { Direction = "S" }) -- lv hospital II s
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12930, 2089, 0, 2, 3, { Direction = "S" }) -- lv hospital II s
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12955, 2089, 0, 2, 3, { Direction = "S" }) -- lv hospital II s
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12957, 2090, 0, 7, 3, { Direction = "S" }) -- lv hospital II s
				
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12923, 2089, 0, 7, 3, { Direction = "S" }) -- lv hospital II s
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12930, 2088, 0, 2, 3, { Direction = "S" }) -- lv hospital II s
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12955, 2088, 0, 2, 3, { Direction = "S" }) -- lv hospital II s
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12957, 2089, 0, 7, 3, { Direction = "S" }) -- lv hospital II s

				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12984, 2006, 0, 3, 4, { Direction = "E" }) -- lv hospital II E
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12984, 2011, 0, 3, 4, { Direction = "E" }) -- lv hospital II E
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12984, 2016, 0, 3, 7, { Direction = "E" }) -- lv hospital II E
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12984, 2025, 0, 3, 6, { Direction = "E" }) -- lv hospital II E
				-- spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12984, 2033, 0, 3, 2, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12983, 2006, 0, 3, 4, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12983, 2011, 0, 3, 4, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12983, 2016, 0, 3, 7, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12983, 2025, 0, 3, 6, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12983, 2033, 0, 3, 2, { Direction = "E" }) -- lv hospital II E
				
				
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12963, 2003, 0, 15, 3, { Direction = "N" }) -- lv hospital II N
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12980, 2003, 0, 4, 3, { Direction = "N" }) -- lv hospital II N
				
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12973, 2052, 0, 3, 2, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12973, 2056, 0, 3, 5, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12973, 2063, 0, 3, 5, { Direction = "E" }) -- lv hospital II E
				spawnWorld:registerVehiclesZone( "pa_wheelchairs", "ParkingStall", 12973, 2070, 0, 3, 2, { Direction = "E" }) -- lv hospital II E
				
								
				-- spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12311, 1293, 0, 18, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12311, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12313, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12315, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12317, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12319, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12321, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12323, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12325, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12327, 1293, 0, 2, 3, { Direction = "N" }) -- lv strip club n	
				
				-- spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12336, 1275, 0, 3, 12, { Direction = "W" }) -- lv strip club w				
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12336, 1275, 0, 3, 2, { Direction = "W" }) -- lv strip club w			
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12336, 1277, 0, 3, 2, { Direction = "W" }) -- lv strip club w			
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12336, 1279, 0, 3, 2, { Direction = "W" }) -- lv strip club w			
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12336, 1281, 0, 3, 2, { Direction = "W" }) -- lv strip club w			
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12336, 1283, 0, 3, 2, { Direction = "W" }) -- lv strip club w			
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12336, 1285, 0, 3, 2, { Direction = "W" }) -- lv strip club w
				
				-- spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12076, 6795, 0, 3, 12, { Direction = "W" }) -- twiggys bar w
				
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12076, 6795, 0, 3, 2, { Direction = "W" }) -- twiggys bar w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12076, 6797, 0, 3, 2, { Direction = "W" }) -- twiggys bar w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12076, 6799, 0, 3, 2, { Direction = "W" }) -- twiggys bar w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12076, 6801, 0, 3, 2, { Direction = "W" }) -- twiggys bar w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12076, 6803, 0, 3, 2, { Direction = "W" }) -- twiggys bar w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 12076, 6805, 0, 3, 2, { Direction = "W" }) -- twiggys bar w		

				
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 11586, 9293, 0, 3, 2, { Direction = "W" }) -- drug shack w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 11586, 9295, 0, 3, 2, { Direction = "W" }) -- drug shack w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 11586, 9297, 0, 3, 2, { Direction = "W" }) -- drug shack w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 11586, 9299, 0, 3, 2, { Direction = "W" }) -- drug shack w
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 11586, 9301, 0, 3, 2, { Direction = "W" }) -- drug shack w
				
				
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 10757, 10557, 0, 2, 3, { Direction = "N" }) -- rusty rifle n
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 10759, 10557, 0, 2, 3, { Direction = "N" }) -- rusty rifle n	
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 10761, 10557, 0, 2, 3, { Direction = "N" }) -- rusty rifle n	
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 10763, 10557, 0, 2, 3, { Direction = "N" }) -- rusty rifle n	
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 10765, 10557, 0, 2, 3, { Direction = "N" }) -- rusty rifle n	
				spawnWorld:registerVehiclesZone( "pa_bikers", "ParkingStall", 10767, 10557, 0, 2, 3, { Direction = "N" }) -- rusty rifle n		
				
				
				spawnWorld:registerVehiclesZone( "pa_ranger_carts", "ParkingStall", 4609, 8594, 0, 3, 2, { Direction = "W" }) -- ranger lodge shed
				-- spawnWorld:registerVehiclesZone( "pa_ranger_carts", "ParkingStall", 4609, 8599, 0, 3, 2, { Direction = "W" }) -- ranger lodge shed
			end
		end
		
	end	
		
		
		Events.OnLoadMapZones.Add(PA_golfcart_Zones)


end
