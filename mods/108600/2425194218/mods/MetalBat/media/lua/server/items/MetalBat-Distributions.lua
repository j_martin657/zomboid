require 'Items/ProceduralDistributions'

table.insert(ProceduralDistributions["list"]["SportStorageBats"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["SportStorageBats"].items, 10);
table.insert(ProceduralDistributions["list"]["SportStorageBats"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["SportStorageBats"].items, 10);
table.insert(ProceduralDistributions["list"]["SportStorageBats"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["SportStorageBats"].items, 10);

table.insert(ProceduralDistributions["list"]["WardrobeMan"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["WardrobeMan"].items, 0.04);

table.insert(ProceduralDistributions["list"]["WardrobeManClassy"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["WardrobeManClassy"].items, 0.025);

table.insert(ProceduralDistributions["list"]["WardrobeRedneck"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["WardrobeRedneck"].items, 0.05);

table.insert(ProceduralDistributions["list"]["WardrobeWoman"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["WardrobeWoman"].items, 0.04);

table.insert(ProceduralDistributions["list"]["WardrobeWomanClassy"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["WardrobeWomanClassy"].items, 0.025);

table.insert(ProceduralDistributions["list"]["BarCounterWeapon"]["junk"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["BarCounterWeapon"]["junk"].items, 25);

table.insert(ProceduralDistributions["list"]["CrateSports"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["CrateSports"].items, 1);

table.insert(ProceduralDistributions["list"]["MeleeWeapons"].items, "MetalBat.MetalBat");
table.insert(ProceduralDistributions["list"]["MeleeWeapons"].items, 5);