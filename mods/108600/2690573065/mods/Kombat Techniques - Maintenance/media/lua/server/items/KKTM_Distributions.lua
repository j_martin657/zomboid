require "Items/ProceduralDistributions"

--Bookstore (BookstoreBooks)
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKTM.BookMaintenance1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKTM.BookMaintenance2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKTM.BookMaintenance3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKTM.BookMaintenance4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "KKTM.BookMaintenance5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.1);

--Post Office (PostOfficeBooks)
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKTM.BookMaintenance1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKTM.BookMaintenance2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKTM.BookMaintenance3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKTM.BookMaintenance4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "KKTM.BookMaintenance5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 0.1);

--Library Books (LibraryBooks)
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKTM.BookMaintenance1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.45);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKTM.BookMaintenance2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.35);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKTM.BookMaintenance3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.25);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKTM.BookMaintenance4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.15);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "KKTM.BookMaintenance5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 0.05);

--Living Room(LivingRoomShelf)
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKTM.BookMaintenance1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.25);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKTM.BookMaintenance2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKTM.BookMaintenance3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.15);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKTM.BookMaintenance4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.1);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "KKTM.BookMaintenance5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.05);