SkillBook["Maintenance"] = {};
SkillBook["Maintenance"].perk = Perks.Maintenance;
SkillBook["Maintenance"].maxMultiplier1 = 1.5;
SkillBook["Maintenance"].maxMultiplier2 = 2.5;
SkillBook["Maintenance"].maxMultiplier3 = 4;
SkillBook["Maintenance"].maxMultiplier4 = 6;
SkillBook["Maintenance"].maxMultiplier5 = 8;