require "TCMusicDefenitions"

	GlobalMusic["VinylArtBlakeyAndTheJazzMessengersMoanin'(1959)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylBeethovenSymphonieNr9"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylBlackSabbathParanoid(1970)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylBlondie'sHits"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylDeepPurpleMachineHead(1972)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylDionandtheBelmonts24OriginalClassics"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylDrDreTheChronic(1992)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylDvorak's9thSymphony"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylELOTime(1981)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylErikSatieGymnopedie"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylFranzSchubertSymphonyNo7"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylGiorgioMoroderFromHeretoEternity(1977)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylJeffWayne'sWarOfTheWorlds(1978)Part1"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylJeffWayne'sWarOfTheWorlds(1978)Part2"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylJohnColtraneALoveSupreme(1965)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylJoyDivisionUnknownPleasures(1979)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylKinoGruppakrovi(1988)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylMilesDavisKindofBlue(1959)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylMomokoKikuchiAdventure(1986)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylNWAStraightOuttaCompton(1988)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylPinkFloydTheDarkSideOfTheMoon(1973)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylRunDMCRaisingHell(1986)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylSexPistolsNeverMindTheBollocks(1977)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylTchaikovskySymphonyNo5"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylTheBestOfChetBakerSings"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylTheCureSeventeenSeconds(1980)"] = "tsarcraft_music_01_63"
	GlobalMusic["VinylWuTangClanEntertheWuTang(1993)"] = "tsarcraft_music_01_63"

