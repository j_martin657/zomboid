local tweaksOptions = {};
tweaksOptions.modOptions = {};

tweaksOptions.configOptions = {
    options_data = {
        essSac = {
            getText("IGUI_ESQ_COMMON_UI_OFF"),
            getText("IGUI_ESQ_COMMON_UI_ON"),
            getText("IGUI_ESQ_COMMON_UI_ON") .. " " .. getText("IGUI_mo_esSacRespect"),

            name = getText("IGUI_ESQ_COMMON_UI_ON") .. "/" .. getText("IGUI_ESQ_COMMON_UI_OFF"),
            default = 2,
        },
    },
    mod_id = "ExtraSauceSac",
    mod_fullname = getText("IGUI_mo_esSacName"),
    mod_shortname = getText("IGUI_mo_esSacName"),
}

function tweaksOptions.getOption(infoOption)
    if (infoOption == "sacOn") then return tweaksOptions.modOptions.options.essSac > 1 end;
    if (infoOption == "sacIgnore") then return tweaksOptions.modOptions.options.essSac == 3 end;
end

if ModOptions and ModOptions.getInstance then
    tweaksOptions.modOptions = ModOptions:getInstance(tweaksOptions.configOptions);
end

return tweaksOptions;