SkillBook["Fitness"] = {};
SkillBook["Fitness"].perk = Perks.Fitness;
SkillBook["Fitness"].maxMultiplier1 = 2;
SkillBook["Fitness"].maxMultiplier2 = 3;
SkillBook["Fitness"].maxMultiplier3 = 5;
SkillBook["Fitness"].maxMultiplier4 = 8;
SkillBook["Fitness"].maxMultiplier5 = 10;

SkillBook["Strength"] = {};
SkillBook["Strength"].perk = Perks.Strength;
SkillBook["Strength"].maxMultiplier1 = 2;
SkillBook["Strength"].maxMultiplier2 = 3;
SkillBook["Strength"].maxMultiplier3 = 5;
SkillBook["Strength"].maxMultiplier4 = 8;
SkillBook["Strength"].maxMultiplier5 = 10;

SkillBook["Sprinting"] = {};
SkillBook["Sprinting"].perk = Perks.Sprinting;
SkillBook["Sprinting"].maxMultiplier1 = 2;
SkillBook["Sprinting"].maxMultiplier2 = 3;
SkillBook["Sprinting"].maxMultiplier3 = 5;
SkillBook["Sprinting"].maxMultiplier4 = 8;
SkillBook["Sprinting"].maxMultiplier5 = 10;

SkillBook["Lightfooted"] = {};
SkillBook["Lightfooted"].perk = Perks.Lightfoot;
SkillBook["Lightfooted"].maxMultiplier1 = 2;
SkillBook["Lightfooted"].maxMultiplier2 = 3;
SkillBook["Lightfooted"].maxMultiplier3 = 5;
SkillBook["Lightfooted"].maxMultiplier4 = 8;
SkillBook["Lightfooted"].maxMultiplier5 = 10;

SkillBook["Nimble"] = {};
SkillBook["Nimble"].perk = Perks.Nimble;
SkillBook["Nimble"].maxMultiplier1 = 2;
SkillBook["Nimble"].maxMultiplier2 = 3;
SkillBook["Nimble"].maxMultiplier3 = 5;
SkillBook["Nimble"].maxMultiplier4 = 8;
SkillBook["Nimble"].maxMultiplier5 = 10;

SkillBook["Sneaking"] = {};
SkillBook["Sneaking"].perk = Perks.Sneak;
SkillBook["Sneaking"].maxMultiplier1 = 2;
SkillBook["Sneaking"].maxMultiplier2 = 3;
SkillBook["Sneaking"].maxMultiplier3 = 5;
SkillBook["Sneaking"].maxMultiplier4 = 8;
SkillBook["Sneaking"].maxMultiplier5 = 10;

SkillBook["Axe"] = {};
SkillBook["Axe"].perk = Perks.Axe;
SkillBook["Axe"].maxMultiplier1 = 3;
SkillBook["Axe"].maxMultiplier2 = 5;
SkillBook["Axe"].maxMultiplier3 = 8;
SkillBook["Axe"].maxMultiplier4 = 12;
SkillBook["Axe"].maxMultiplier5 = 16;

SkillBook["Blunt"] = {};
SkillBook["Blunt"].perk = Perks.Blunt;
SkillBook["Blunt"].maxMultiplier1 = 3;
SkillBook["Blunt"].maxMultiplier2 = 5;
SkillBook["Blunt"].maxMultiplier3 = 8;
SkillBook["Blunt"].maxMultiplier4 = 12;
SkillBook["Blunt"].maxMultiplier5 = 16;

SkillBook["SmallBlunt"] = {};
SkillBook["SmallBlunt"].perk = Perks.SmallBlunt;
SkillBook["SmallBlunt"].maxMultiplier1 = 3;
SkillBook["SmallBlunt"].maxMultiplier2 = 5;
SkillBook["SmallBlunt"].maxMultiplier3 = 8;
SkillBook["SmallBlunt"].maxMultiplier4 = 12;
SkillBook["SmallBlunt"].maxMultiplier5 = 16;

SkillBook["LongBlade"] = {};
SkillBook["LongBlade"].perk = Perks.LongBlade;
SkillBook["LongBlade"].maxMultiplier1 = 3;
SkillBook["LongBlade"].maxMultiplier2 = 5;
SkillBook["LongBlade"].maxMultiplier3 = 8;
SkillBook["LongBlade"].maxMultiplier4 = 12;
SkillBook["LongBlade"].maxMultiplier5 = 16;

SkillBook["SmallBlade"] = {};
SkillBook["SmallBlade"].perk = Perks.SmallBlade;
SkillBook["SmallBlade"].maxMultiplier1 = 3;
SkillBook["SmallBlade"].maxMultiplier2 = 5;
SkillBook["SmallBlade"].maxMultiplier3 = 8;
SkillBook["SmallBlade"].maxMultiplier4 = 12;
SkillBook["SmallBlade"].maxMultiplier5 = 16;

SkillBook["Spear"] = {};
SkillBook["Spear"].perk = Perks.Spear;
SkillBook["Spear"].maxMultiplier1 = 3;
SkillBook["Spear"].maxMultiplier2 = 5;
SkillBook["Spear"].maxMultiplier3 = 8;
SkillBook["Spear"].maxMultiplier4 = 12;
SkillBook["Spear"].maxMultiplier5 = 16;

SkillBook["Maintenance"] = {};
SkillBook["Maintenance"].perk = Perks.Maintenance;
SkillBook["Maintenance"].maxMultiplier1 = 3;
SkillBook["Maintenance"].maxMultiplier2 = 5;
SkillBook["Maintenance"].maxMultiplier3 = 8;
SkillBook["Maintenance"].maxMultiplier4 = 12;
SkillBook["Maintenance"].maxMultiplier5 = 16;

SkillBook["Aiming"] = {};
SkillBook["Aiming"].perk = Perks.Aiming;
SkillBook["Aiming"].maxMultiplier1 = 3;
SkillBook["Aiming"].maxMultiplier2 = 5;
SkillBook["Aiming"].maxMultiplier3 = 8;
SkillBook["Aiming"].maxMultiplier4 = 12;
SkillBook["Aiming"].maxMultiplier5 = 16;

SkillBook["Reloading"] = {};
SkillBook["Reloading"].perk = Perks.Reloading;
SkillBook["Reloading"].maxMultiplier1 = 3;
SkillBook["Reloading"].maxMultiplier2 = 5;
SkillBook["Reloading"].maxMultiplier3 = 8;
SkillBook["Reloading"].maxMultiplier4 = 12;
SkillBook["Reloading"].maxMultiplier5 = 16;

