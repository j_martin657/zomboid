local MN_ContextDisassemble = ISWorldMenuElements.ContextDisassemble
local wornItemsBeforeTable = {}


ISWorldMenuElements.ContextDisassemble = function()
	local MN_ContextDisassemble_2 = MN_ContextDisassemble()
	MN_disassemble = MN_ContextDisassemble_2.disassemble

	MN_ContextDisassemble_2.disassemble = function(data, v)
        local player = data.player
        local playerNum = data.playerNum
        local inventory = data.inventory

        --기존 장착한 물건 확인
        primaryHandItem = player:getPrimaryHandItem()
        secondaryHandItem = player:getSecondaryHandItem()

        --기존 입은옷 확인
        local wornItems = player:getWornItems()
        for i=0, wornItems:size()-1 do
            local wornItem = wornItems:get(i)
            wornItemsBeforeTable[wornItem:getLocation()] = wornItem:getItem()
        end
        
        --도구 위치 확인
        tool1 = v.resultScrap.haveTool

        if type(tool1) == "boolean" then
            tool1Type = false
        else
            tool1Type = true
            tool1Location = tool1:getContainer()
        end
        
        tool2 = v.resultScrap.haveTool2

        if type(tool2) == "boolean" then
            tool2Type = false
        else
            tool2Type = true
            tool2Location = tool2:getContainer()
        end

        --분해 실행
        local mainAction = MN_disassemble(data, v)

        return mainAction
	end

	return MN_ContextDisassemble_2
end


function MN_weaponBack_playerUpdate(_player) --분해 중일때 행동예약
    if _player:hasTimedActions() and
            _player:getCharacterActions():get(0):getMetaType() == "ISMoveablesAction" then
        --현재옷 체크
        local wornItemsAfter = _player:getWornItems()
        local wornItemsAfterTable = {}

        for i=0, wornItemsAfter:size()-1 do
            local wornItem = wornItemsAfter:get(i)
            local Location = wornItem:getLocation()
            local item = wornItem:getItem()

            wornItemsAfterTable[Location] = item
        end

        --기존 장착한 물건 다시 장착
        if primaryHandItem then
            ISTimedActionQueue.add(ISEquipWeaponAction:new(_player, primaryHandItem, 50, true, primaryHandItem == secondaryHandItem))
        end
        if secondaryHandItem and primaryHandItem ~= secondaryHandItem then
            ISTimedActionQueue.add(ISEquipWeaponAction:new(_player, secondaryHandItem, 50, false, false))
        end

        --도구 원래위치로 이동
        local inventory = _player:getInventory()

        if tool1Type then
            ISTimedActionQueue.add(ISInventoryTransferAction:new(_player, tool1, inventory, tool1Location))
        end
        if tool2Type then
            ISTimedActionQueue.add(ISInventoryTransferAction:new(_player, tool2, inventory, tool2Location))
        end
        
        --기존옷 다시 장착
        for k, v in pairs(wornItemsBeforeTable) do
            if v ~= wornItemsAfterTable[k] then
                ISTimedActionQueue.add(ISWearClothing:new(_player, v, 50))
            end
        end

        --변수 초기화
        wornItemsBeforeTable = {}

        primaryHandItem = nil
        secondaryHandItem = nil

        tool1 = nil
        tool1Type = nil
        tool1Location = nil

        tool2 = nil
        tool2Type = nil
        tool2Location = nil
    end
end

Events.OnPlayerUpdate.Add(MN_weaponBack_playerUpdate)