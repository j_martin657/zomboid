require "ISBaseObject"
require "Reloading/ISReloadUtil"
require "Reloading/ISRackAction"
require "Reloading/ISReloadAction"


local function ejectBrass(player, weapon)
	
	-----------------------
	--  OPTIONS BOX 19   --
	-----------------------
	if (GUNFIGHTER.OPTIONS.options.dropdown19) > 1 then		-- 1 EQUALS NO BRASS EJECTION

		local attacker	= player;
		local weapon 	= weapon;
		local name		= weapon:getType();
		local ammo		= weapon:getAmmoType();
		local type		= weapon:getWeaponReloadType();
		local inv		= player:getInventory();
		local ejected	= nil
		local spent		= weapon:getModData().spentammo
		if	spent		==  nil then
			spent		= 0
		end

		if 	ammo 		== "Base.Bullets22" then
			ejected	= "Brass22"
		elseif 	ammo	== "Base.Bullets57" then
			ejected	= "Brass57"
		elseif	ammo	== "Base.Bullets380" then
			ejected	= "Brass380"
		elseif	ammo	== "Base.Bullets38" then
			ejected	= "Brass38"
		elseif	ammo	== "Base.Bullets9mm" then
			ejected	= "Brass9"
		elseif	ammo	== "Base.Bullets45" then
			ejected	= "Brass45"
		elseif	ammo	== "Base.Bullets357" then
			ejected	= "Brass357"
		elseif	ammo	== "Base.Bullets45LC" then
			ejected	= "Brass45LC"
		elseif	ammo	== "Base.Bullets44" then
			ejected	= "Brass44"
		elseif	ammo	== "Base.Bullets50MAG" then
			ejected 	= "Brass50MAG"
		elseif	ammo	== "Base.Bullets4570" then
			ejected	= "Brass4570"
		elseif	ammo	== "Base.223Bullets" then
			ejected	= "Brass223"
		elseif	ammo	== "Base.556Bullets" then
			ejected	= "Brass556"
		elseif	ammo	== "Base.545x39Bullets" then
			ejected	= "Brass545x39"
		elseif	ammo	== "Base.762x39Bullets" then
			ejected	= "Brass762x39"
		elseif	ammo	== "Base.308Bullets" then
			ejected	= "Brass308"
		elseif	ammo	== "Base.762x51Bullets" then
			ejected	= "Brass762x51"
		elseif	ammo	== "Base.762x54rBullets" then
			ejected	= "Brass762x54r"
		elseif	ammo	== "Base.3006Bullets" then
			ejected	= "Brass3006"
		elseif	ammo	== "Base.410gShotgunShells" then
			ejected	= "Hull410g"
		elseif	ammo	== "Base.20gShotgunShells" then
			ejected	= "Hull20g"
		elseif	ammo	== "Base.ShotgunShells" then
			ejected	= "Hull12g"
		elseif	ammo	== "Base.10gShotgunShells" then
			ejected	= "Hull10g"
		elseif	ammo	== "Base.4gShotgunShells" then
			ejected	= "Hull4g"
		elseif	ammo	== "Base.50BMGBullets" then
			ejected	= "Brass50BMG"
		end

		if	type == "handgun" or 
			type == "boltaction" or 
			type == "leveraction" or
			type == "boltactionnomag" or
			type == "shotgun" then		-- YES EJECT

			if ZombRand(11) < (GUNFIGHTER.OPTIONS.options.dropdown19) then
				if ejected ~= nil then
					player:getCurrentSquare():AddWorldInventoryItem(ejected, 0.0, 0.0, 0.0);
					DebugSay(3,"Brass Ejected")
				else	DebugSay(3,"Not Usable")
				end
			end
			
		else			-- NOT EJECT

		--	weapon:getModData().spentammo = spent + 1
		--	DebugSay(3,tostring(spent+1).."spent shells in gun")

			if ejected ~= nil then
				inv:AddItem(ejected);
				DebugSay(3,"Brass to Inventory")
			end
		end
	end
end


Events.OnPlayerAttackFinished.Add(ejectBrass);
